import numpy as np
from os.path import isfile

class QLearningModelFactory:
    def loadOrInit(self, folder, statesCnt, contorlsCnt):
        if(folder is not None):
            qModelParamsPath = folder + "/qtable.npy"
            if(isfile(qModelParamsPath)):
                return QLearningModel(np.load(qModelParamsPath))
            else:
                return QLearningModel(np.zeros((statesCnt, contorlsCnt), dtype=np.float32))
        else:
            return QLearningModel(np.zeros((statesCnt, contorlsCnt), dtype=np.float32))
    def save(self,qTable, folder):
        np.save(folder + "/qtable", qTable.qTable)

class QLearningModel:
    def __init__(self, qTable):
        self.qTable = qTable

    def getQValues(self, state):
        return self.qTable[state]

    def getQValue(self, state, action):
        return self.qTable[state][action]

    # Q'[s, a] = (1 - α) · Q[s, a] + α · (r + γ · Q[s', argmaxa'(Q[s', a'])])
    def updateQValue(self, previousStateIndex, previousReward, previousActionIndex, currentStateIndex):
        if (previousStateIndex is not None and previousReward is not None and previousActionIndex is not None):
            self.qTable[previousStateIndex, previousActionIndex] = \
            (1-0.3)*self.qTable[previousStateIndex, previousActionIndex] + 0.3*(previousReward + 0.5*max(self.qTable[currentStateIndex]))
