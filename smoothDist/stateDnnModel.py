import tensorflow as tf
import sonnet as snt

class StateDnnModel:

    def __custom_build(self, inputs, is_training, keep_prob):
        """A custom build method to wrap into a sonnet Module."""
        with tf.name_scope("first_conv1"):
            conv2dmod1 = snt.Conv2D(output_channels=32, kernel_shape=4, stride=2, name="conv_2d_1")
            outputs = conv2dmod1(inputs)
            outputs = snt.BatchNorm()(outputs, is_training=is_training)
            outputs = tf.nn.relu(outputs)
            if self.isTraining:
                tf.summary.histogram("weights", conv2dmod1.w)
                tf.summary.histogram("biases", conv2dmod1.b)
                tf.summary.histogram("activation", outputs)

        with tf.name_scope("second_conv2"):
            conv2dmod2 = snt.Conv2D(output_channels=64, kernel_shape=4, stride=2, name="conv_2d_2")
            outputs = conv2dmod2(outputs)
            outputs = snt.BatchNorm()(outputs, is_training=is_training)
            outputs = tf.nn.relu(outputs)
            if self.isTraining:
                tf.summary.histogram("weights", conv2dmod2.w)
                tf.summary.histogram("biases", conv2dmod2.b)
                tf.summary.histogram("activation", outputs)
        outputs = snt.BatchFlatten()(outputs)
        outputs = tf.nn.dropout(outputs, keep_prob=keep_prob)
        outputs = snt.Linear(output_size=10)(outputs)
        outputs = tf.nn.softmax(outputs)
        return outputs

    def getPredictions(self, inputs):
        """Sonnet module. Conv2D -> BatchNorm -> relu -> Conv2D -> BatchNorm ->
        relu -> BatchFlatten -> dropout -> Linear -> softmax"""
        if self.predictionsMod is None:
            self.predictionsMod = snt.Module(self.__custom_build, name='predictions')(inputs, is_training=self.isTraining,keep_prob=tf.constant(0.5))
        return self.predictionsMod

    def __calculateTarget(self, train_model_outputs):
        with tf.name_scope("target_values"):
            if(self.outputDistribution is None):
                self.outputDistribution = tf.get_variable("outputDistribution", dtype=tf.int32, shape=([10]), initializer=tf.ones_initializer())
            compensation =  tf.cast(tf.scalar_mul(4,tf.divide(self.outputDistribution, tf.reduce_sum(self.outputDistribution))), tf.float32)
            compensatedOutput = tf.subtract(train_model_outputs, compensation)

            targetValues = tf.one_hot(tf.argmax(compensatedOutput, output_type=tf.int32, axis=1), 10)
            compensationCorrection = tf.cast(tf.reduce_sum(targetValues, axis=0), tf.int32)
            self.outputDistribution = self.outputDistribution.assign_add(compensationCorrection)
            return targetValues

    def __getLoss(self, inputs):
        train_model_outputs = self.getPredictions(inputs)
        targetValues = self.__calculateTarget(train_model_outputs)
        lossVal = tf.subtract(targetValues, train_model_outputs)
        return tf.nn.l2_loss(lossVal, name="lossfunc")

    def getOptimizer(self, inputs):
        if self.optimizerMod is None:
            loss = self.__getLoss(inputs)
            with tf.name_scope("optimizer"):
                if self.isTraining:
                    tf.summary.scalar("loss", loss)
                self.optimizerMod = tf.train.GradientDescentOptimizer(learning_rate=1e-10, name="optimizer").minimize(loss)

        return self.optimizerMod

    def __init__(self, isTraining):
        self._predictions = None
        self._optimizer = None
        self.outputDistribution = None
        self.isTraining = isTraining
        self.predictionsMod = None
        self.optimizerMod = None
