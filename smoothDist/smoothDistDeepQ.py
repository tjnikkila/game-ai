import gym
import universe
from pprint import pprint
import os
from os.path import isfile
import random
import numpy as np
import tensorflow as tf
import sonnet as snt
from random import randint
from logging import INFO
from tensorflow.python.platform import tf_logging
from smoothDist.stateDnnModel import StateDnnModel
from smoothDist.qLearningModel import QLearningModelFactory
from util.benchmark import MethodBenchmark
from findActionPattern.bestPatterns import forward, patterns
from util.observationFunctions import getFocusedDownScaledImage, getDownScaledImage, getRgbObs

folderName = './smoothDist/modelParams'
fileBaseName = 'dnn'

class SmoothDistDeepQ:
    def run(self, iterationCount, raceCount, focusObservation, observer):
        # batchSize has to be one in this implementation.
        batchSize = 1
        fetchPerCommand = 5
        commandsPerPattern = 5

        dnnModTrain = StateDnnModel(True)
        trainInputs = tf.placeholder(tf.float32, shape=(batchSize, 70, 100, 3), name="x")
        train_step = dnnModTrain.getOptimizer(trainInputs)
        saver = tf.train.Saver()

        env = gym.make('flashgames.CoasterRacer-v0')
        env.configure(remotes=1)
        observations = env.reset()

        with tf.Session() as trainSess:
            tf_logging.set_verbosity(INFO)

            tf_logging.info("run started")
            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):
                train_model_outputs = dnnModTrain.getPredictions(trainInputs)

            with tf.name_scope("global_initializer"):
                trainSess.run(tf.global_variables_initializer())

            tfModelParamsPath = folderName + '/' + fileBaseName + ".ckpt"
            if(isfile(tfModelParamsPath + ".meta")):
                saver.restore(trainSess, tfModelParamsPath)


            merged_summary = tf.summary.merge_all()

            writer = tf.summary.FileWriter(os.getcwd() + "/model/board/1", flush_secs=60)
            writer.add_graph(trainSess.graph)

            qTable = QLearningModelFactory().loadOrInit(folderName, 10,5)

            exploitationProb = 0.1
            previousStateIndex = None
            previousActionIndex = None
            previousReward = None
            cumulativeRewardForCommand = 0
            actionPatternIndex = 0
            currentActionIndex = 0
            rewards = None
            methodBenchmark = MethodBenchmark(raceCount)

            for i in range(iterationCount):
                if(i > 1 and observations[0] is not None and rewards is not None):
                    assert(len(rewards) == 1)
                    reward = rewards[0]

                    cumulativeRewardForCommand += reward
                    if i % fetchPerCommand == 0:
                        if i % (fetchPerCommand * commandsPerPattern) == 0:
                            if(i / (fetchPerCommand * commandsPerPattern) > 300):
                                # if exploitationProb is big from the begin then zero index dominates in rewards
                                exploitationProb = 0.95

                            if(focusObservation == True):
                                observedVision = np.array([getFocusedDownScaledImage(observations)])
                            else:
                                observedVision = np.array([getDownScaledImage(observations)])
                            predictions, train_stp = trainSess.run([train_model_outputs, train_step], feed_dict = {trainInputs: observedVision})

                            if(i % (fetchPerCommand * commandsPerPattern * 15) == 0):
                                s = trainSess.run(merged_summary, feed_dict = {trainInputs: observedVision})
                                writer.add_summary(s, i)
                            if(i % (fetchPerCommand * commandsPerPattern * 100)== 0):
                                tf_logging.info(str(i) + " iteration")
                                observationImages = tf.summary.image('observation', observedVision)
                                o = trainSess.run(observationImages, feed_dict = {trainInputs: observedVision})
                                writer.add_summary(o, i)

                            currentStates = np.argmax(predictions, axis=1) # max index -> index of state
                            assert(len(currentStates) == 1)
                            currentState = currentStates[0]

                            actionValues = qTable.getQValues(currentState)

                            # get next action. Policy
                            if random.random() > exploitationProb:
                                actionPatternIndex = randint(0, len(patterns) - 1)
                            else:
                                actionPatternIndex = np.argmax(actionValues)

                            print("action index -> " + str(actionPatternIndex) + ". current state -> " + str(currentState) + ". Iteration count " + str(i) + "/" + str(iterationCount))

                            qTable.updateQValue(previousStateIndex, previousReward, previousActionIndex, currentState)
                            previousStateIndex = currentState
                            previousActionIndex = actionPatternIndex
                            previousReward = cumulativeRewardForCommand
                            cumulativeRewardForCommand = 0
                            currentActionIndex = 0

                        # Ocassionaly breaks down when old game ends and new starts. Index grows too big.
                        # Dirty fix is to check that idex is not too big.
                        currentActionIndex = currentActionIndex % 5 # there are 5 actions to take in pattern
                        takeAction = patterns[actionPatternIndex][currentActionIndex]

                        if observer.observing(observations, cumulativeRewardForCommand, i, currentActionIndex, {"patternIndex": int(actionPatternIndex)}) is False:
                            break
                        currentActionIndex += 1

                    if(i % iterationCount == iterationCount - 1):
                        saver.save(trainSess, folderName + '/' + fileBaseName + ".ckpt")
                        QLearningModelFactory().save(qTable, folderName)

                else:
                    takeAction = forward

                action_n = [takeAction for ob in observations]
                observations, rewards, done_n, info = env.step(action_n)
                benchmarkingDone = methodBenchmark.render(done_n, rewards, observations)
                if benchmarkingDone:
                    saver.save(trainSess, folderName + '/' + fileBaseName + ".ckpt")
                    QLearningModelFactory().save(qTable, folderName)
                    break

            methodBenchmark.printResults()
