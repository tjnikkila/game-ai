### Smooth Two Layer Conv2d Distribution with Q-Learning

This solution is done to get hands on experiences into deep reinforcement learning. Solution is the first solution that came to mind and is not based on any studies or whitepapers.

Overall logic is as follows:

1. Get new observation and reward once in every 25 iteration.
2. Get current state from dnn(two conv2d). Update dnn model during the same call.
3. Get actions Q-values for current state.
4. Get next action pattern from policy. Give state's Q-values as input. At the begin policy is more explorative and after a while it gets more exploitative.
5. Update previous state Q-values.
6. take action.
7. go back to step 1.

State is predicted on step two. State is predicted only based on latest observation. The idea behind was to categorize observation in one of the ten states. Predictions tried to converge to predict the same state every time. To avoid this, model got some penalty to loss function. More unequally it predicted states, more penalty it got. Dnn graph is illustrated at `Image 1`.

_Image 1_
![alt text](smoothDist/documentation/dnn_model.png "Dnn Model")

Results: As many can guess, results were not too good. For example `Image 2` illustrates how loss values developed over first 10 races. Model improved for the first 50k iterations. `Image 3` illustrates how loss values developed over the races between 10 to 20.

_Image 2_
![alt text](smoothDist/documentation/optimizer_loss.png "Dnn Loss")

_Image 3_
![alt text](smoothDist/documentation/optimizer_loss_10_to_20.png "Dnn Loss. Usual case, from 20 to 20 races")
