import scipy
from scipy.stats import pearsonr
import numpy as np
class CalculateSimilarity:

    # suunta: numpy.corrcoef([6,0,1], [6,1,1])[0,1]
    # etaisyys: numpy.linalg.norm([0,3])

    def stringToCntVec(self, sourceStr):
        # TODO: mika korvertoidaan mihin pitaa tulla rakentajan parametrina
        chars = "frl"
        conversions = { "s":"fr", "m":"fl" }
        directions = sourceStr
        for convKey in list(conversions.keys()):
            directions = directions.replace(convKey, conversions[convKey])

        charCount = []
        for char in chars:
          count = directions.count(char)
          charCount.append(count)

        return charCount

    def localizePatternId(self, patternId):
        return ''.join(sorted(patternId))

    # returns value between 1 and 0.
    def getSimilar(self, patterns, patternId):
        charCount = self.stringToCntVec(patternId)

        # bestDist = 100
        # bestPattern = None
        for ptrn in patterns.values():
            ptrnVec = self.stringToCntVec(ptrn['patternId'])
            if(charCount == ptrnVec):
                return ptrn

            # TODO: lisaa distance lasku myohemmin.
            # difference = np.subtract(charCount, ptrnVec)
            # distanceSimilarity = np.linalg.norm(difference)
            # if(distanceSimilarity < bestDist):
            #     bestPattern = ptrn
            #     bestDist = distanceSimilarity
        return None

# patterns = [ \
#     {'patternId': "fffff" }, \
#     {'patternId': "mmssl" }, \
#     {'patternId': "mmmmm" }]
# f = CalculateSimilarity()
# print(f.getSimilar(patterns, {'patternId': "mmmmm" }))
# print(f.getSimilar(patterns, {'patternId': "fmlmr" }))
# print(f.getSimilar(patterns, {'patternId': "ffffs" }))
