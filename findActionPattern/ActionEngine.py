import numpy as np
from random import randint

class ActionState:
    unchanged = "unchanged"
    changed = "changed"
    ended = "ended"

def toResponse(actionState, action, reward):
    return {'actionState': actionState, 'action': action, 'reward': reward}

class ActionEngineInterface:
    currentPatternId = None
    successfulPatterns = np.array(["",0.0])
    loopCount = 0
    currentMoveInPattern = 0
    update = False
    bestActions = []
    newGoodActions = []
    patternActions = None
    rewardsThisPattern = 0
    left = [('KeyEvent', 'ArrowUp', False),('KeyEvent', 'ArrowLeft', True), ('KeyEvent', 'ArrowRight', False)]
    right = [('KeyEvent', 'ArrowUp', False),('KeyEvent', 'ArrowLeft', False), ('KeyEvent', 'ArrowRight', True)]
    leftForward = [('KeyEvent', 'ArrowUp', True) ,('KeyEvent', 'ArrowLeft', True), ('KeyEvent', 'ArrowRight', False)]
    rightForward = [('KeyEvent', 'ArrowUp', True) ,('KeyEvent', 'ArrowLeft', False), ('KeyEvent', 'ArrowRight', True)]
    Forward = [('KeyEvent', 'ArrowUp', True) ,('KeyEvent', 'ArrowLeft', False), ('KeyEvent', 'ArrowRight', False)]
    legalActions = [{'name' : 'f', 'KeyEvent': Forward}, {'name' : 'l','KeyEvent': left}, {'name' : 'r', 'KeyEvent':right},{'name' : 'm','KeyEvent':leftForward}, {'name' : 's', 'KeyEvent':rightForward}]
    nameToActionMap = { \
        'f':{'name' : 'f', 'KeyEvent': Forward}, \
        'l': {'name' : 'l','KeyEvent': left}, \
        'r': {'name' : 'r', 'KeyEvent':right}, \
        'm': {'name' : 'm','KeyEvent':leftForward}, \
        's': {'name' : 's', 'KeyEvent':rightForward}}

    def __init__(self, rendersPerMove, movesPerPattern, saveWhenRenderCnt, patternReward):
        self.rendersPerMove = rendersPerMove
        self.movesPerPattern = movesPerPattern
        self.update = False
        self.child = None
        self.lastAction = self.Forward
        self.patternReward = patternReward
        self.saveWhenRenderCnt = saveWhenRenderCnt

    def getName(obj):
        return obj['name']

    # adds reward to current pattern. Should be called after every pattern.
    # Palauttaa {State, Action, reward}. State on joko, Unchanged, Changed tai Ended
    def render(self, rewards):
        self.loopCount += 1
        if(self.loopCount > 1 and self.loopCount % self.rendersPerMove == 0):
            # XXX: is changed state obsolete after ended?
            state = ActionState.changed
            action = None

            newReward = self.patternReward.render(np.sum(rewards))
            if (self.currentMoveInPattern == self.movesPerPattern - 2):
                self.currentMoveInPattern = 0
                state = ActionState.ended
                action = self.patternActions[self.currentMoveInPattern]['KeyEvent']
            else:
                action = self.patternActions[self.currentMoveInPattern]['KeyEvent']
                self.currentMoveInPattern += 1

            if(self.loopCount % self.saveWhenRenderCnt == 0):
                self.patternReward.savePatterns()

            self.lastAction = action
            return toResponse(state, action, newReward)

        elif (self.loopCount == 1):
            return toResponse(ActionState.changed, self.lastAction, 0)
        else:
            newReward = self.patternReward.render(np.sum(rewards))
            return toResponse(ActionState.unchanged, self.lastAction, newReward)

    def startNewPattern(self, patternId):
        def nameToAction(name):
            return self.nameToActionMap[name]

        self.patternActions = list(nameToAction(x) for x in list(patternId))
        self.currentPatternId = patternId
        self.patternReward.startPattern(patternId)
        return True

    def getRandomPatternId(self):
        startNr = 0
        nextPattern = ""
        while startNr < self.movesPerPattern:
            startNr += 1
            nextPattern += self.legalActions[randint(0,4)]['name']
        return nextPattern

    def getNextPatternId(self):
        return self.getRandomPatternId()


    def initialAction(self):
        return self.Forward
