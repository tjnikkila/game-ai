#### Background

Having 5 random actions per pattern. Actions are
forward, left, right, forward + left and forward + right. One pattern can have same
action from 0 to 5 times. So there would be 3125 different combinations. Also one
action is to last 5 iterations. Which is around quarter of a second. One patter then lasts
from over one second. Two assumptions were made:
* Re-arranging a pattern would not make car to collide to walls. So `forward,
left` = `left, forward`
* It is in general not good to go left then right then left and right in one second. At least with real cars this would harm performance of going forward. So re-arranging
random order to sorted order probably helps to gain higher speed.

After these assumptions these two patterns come equals:
`forward, left, left + forward, right` == `left + forward, right, forward, left`. Worth
noticing that one action can contain more than one key strokes.

Now we deal with only 50 different combinations. It is much faster to get lots of
statistical data for each pattern than in the original case.

#### Analysing Results

Figure 1 shows how good it
was to turn left and right. Horizontal axis shows how many keystrokes pattern
took to right. So pattern "right, right, left" would take one to right.
Notice that -4 to right is 4 key strokes to left. Vertical axis shows
how many key strokes pattern took forward. Colour is number of times reward was
gained by pattern divided by how many times pattern was used. Looks like
best patterns contain 4 to 5 actions forward and from 0 to 3 actions left or right.

_Figure 1_
![alt text](findActionPattern/analysis/scripts/valueDistributionLeftRightForw.png "Figure 1")

One shortcut to model patterns was aggregate different orders of same actions into one sorted pattern.
I assumed that turning left and right within second would only hurt performance, so
it could be ignored in a name of performance. Figure 1 already hints that this assumption was correct.
Good pattern tends to go straight forward and turn a bit left or right.
I want to test it bit more. If pattern (before sorting) contains two left and two right key strokes,
then it went back and forth from 1 to 3 times. If pattern contains one left and three
right keystrokes, then it went back and forth 1 or 2 times. There were hundred of thousands
patterns before sorting. It is likely that back and forth are distorting results
based on their occurrence probability. Figure 2 illustrates how patterns in different back and forth categories generated value (after aggregation by sorting).
Value is again number of times pattern was used and reward was gained divided by how
many times pattern was used. Average values:
* no back and forth: 0.03179
* max one back and forth: 0.03327
* max two back and forth: 0.03215

_Figure 2_
![alt text](findActionPattern/analysis/scripts/effectOfTurning.png "Figure 2")

Surprisingly "no back and forth" was not the best. It could be because all mainly left
and mainly right but not forward were in this category as well. This may explain the peak in zero
value. Most of the best patterns also fall into category of "no back and forth". This
visualization leaves bit ambiguous message. However I am more eager to move on
than to find better evidences for decisions so far.

Figure 1 was done by dirty [script](findActionPattern/analysis/scripts/valueOfLeftAndRight.py)

Figure 2 was done by dirty [script](findActionPattern/analysis/scripts/effectOfTurning.py)

File [basicPatterns2017-06-24T19:10:12Z.json](findActionPattern/analysis/scripts/basicPatterns2017-06-24T19:10:12Z.json) is build by running `python runActionPatterns.py`.
'r' is right, 'l' is left, 'f' is forward, 's' is right + forward, 'm' is left + forward.

##### Delayed reward

With this particular game, you must drive well for a while before you start gaining reward. Basically good patterns
may build up reward without getting it. Then one bad pattern may get the reward and collapse to wall. Simple and more generic is
better, so the first solution was to ignore this. However, I want to test the effect of this phenomenon. Principal
of delayed reward is to have 5 pattern queue. When new pattern is used, it is put in queue and last one is poped
out. If reward is gained, then all patterns in queue gets the same reward and are popped away from queue.

Following images are as with the case of previous solution. Figures 1 and 3 are telling the same story. Delayed reward (image 3) is more clear with it.
Best patterns are those with mainly going forward and maybe bit of left or right. Also figures 2 and 4 are telling the same stroy.
It looks like category of "no back and forth" is doing bit better than earlier, but the difference is small.
Calculating average values support this conclution:
* no back and forth: 0.07591
* max one back and forth: 0.06908
* max two back and forth: 0.06524

It seems like choosing the simple solution without deferred reward is good enough.

_Figure 3_
![alt text](findActionPattern/analysis/scripts/valueDistributionLeftRightForwDef.png "Figure 3")

_Figure 4_
![alt text](findActionPattern/analysis/scripts/effectOfTurningDef.png "Figure 4")

Data is here: File [secDeferred2017-06-26T05:50:26Z.json](findActionPattern/analysis/scripts/secDeferred2017-06-26T05:50:26Z.json)
