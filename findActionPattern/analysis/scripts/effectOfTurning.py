import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import json
import ast

np.random.seed(0)

# example data
mu = 100  # mean of distribution
sigma = 15  # standard deviation of distribution
x = mu + sigma * np.random.randn(437)
num_bins = 10
fig, (ax0, ax1, ax2) = plt.subplots(ncols=3, figsize=(12, 4))

count0 = []
count1 = []
count2 = []

with open("basicPatterns2017-06-24T19:10:12Z.json") as data_file:
    fileStr = data_file.read()
    datapoints = ast.literal_eval(fileStr)
    # x is angle, y is distance, z is reward
    # pattern id -> m -> lf, s -> rf
    rights = []
    forwards = []
    counts = []
    for pattern in datapoints.values():
        patternId = pattern["patternId"]
        fs = patternId.count('f')
        rs = patternId.count('r')
        ls = patternId.count('l')
        ms = patternId.count('m')
        ss = patternId.count('s')

        turns = min(rs + ss, ls + ms)
        countProjection = pattern["count"] / (pattern["nonRewardedCount"] + pattern["count"])
        #countProjection = pattern["reward"]
        if turns == 0:
            count0.append(countProjection)
        elif turns == 1:
            count1.append(countProjection)
        else:
            count2.append(countProjection)

print(sum(count0)/len(count0))

znp = np.array(count0)
onp = np.array(count1)
tnp = np.array(count2)
n, bins, patches = ax0.hist(znp, num_bins, normed=1)

ax0.set_xlabel('count when gained points / total count')
ax0.set_ylabel('Probability density')
ax0.set_title("No turning back and forth")
print('x0 average ' +  str(sum(count0)/len(count0)))

n, bins, patches = ax1.hist(onp, num_bins, normed=1)
ax1.set_xlabel('count when gained points / total count')
ax1.set_ylabel('Probability density')
ax1.set_title("Back and forth max once")
print('ax1 average ' +  str(sum(count1)/len(count1)))
n, bins, patches = ax2.hist(tnp, num_bins, normed=1)
ax2.set_xlabel('count when gained points / total count')
ax2.set_ylabel('Probability density')
ax2.set_title("Back and forth max twice")
print('ax2 average ' +  str(sum(count2)/len(count2)))
# Tweak spacing to prevent clipping of ylabel
fig.tight_layout()
plt.savefig('effectOfTurning.png')
plt.show()
