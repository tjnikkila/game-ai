import json
import ast
import numpy as np
import matplotlib.pyplot as plt

dx = 1
dy = 1
y,x = np.mgrid[slice(0,5+dy,dy),slice(-5,6+dx,dx)]
z = np.zeros((6,11), dtype=np.double)

with open("basicPatterns2017-06-24T19:10:12Z.json") as data_file:
    fileStr = data_file.read()
    datapoints = ast.literal_eval(fileStr)
    # x is angle, y is distance, z is reward
    # pattern id -> m -> lf, s -> rf
    rights = []
    forwards = []
    counts = []
    for pattern in datapoints.values():
        patternId = pattern["patternId"]
        fs = patternId.count('f')
        rs = patternId.count('r')
        ls = patternId.count('l')
        ms = patternId.count('m')
        ss = patternId.count('s')
        turnsRightProjection = 0
        turnsLeftProjection = 0

        toRight = (ss + rs) - (ms + ls)


        forwardProjection = fs + ss + ms
        countProjection = pattern["count"] / (pattern["nonRewardedCount"] + pattern["count"])
        #print(toRight + 5, forwardProjection/dy, countProjection)
        xInd = int(toRight + 5)
        yInd = int(forwardProjection/dy)
        if(int(forwardProjection/dy) == 0):
            print(int(forwardProjection/dy), countProjection, patternId)
        z[yInd, xInd] = countProjection + z[yInd, xInd]


    print(z[0:1,:])
    z_min, z_max = 0, np.abs(z).max()
    plt.subplot(1, 1, 1)
    plt.pcolor(x, y, z, cmap='RdBu', vmin=z_min, vmax=z_max)
    plt.title('left   right')
    # set the limits of the plot to the limits of the data
    plt.axis([-x.max() + 1, x.max(), 0, y.max()])
    plt.colorbar()

    plt.subplots_adjust(wspace=0.5, hspace=0.5)
    plt.savefig('valueDistributionLeftRightForw.png')
    plt.show()

    #print(list(datapoints.values()))

    # XXX: Summary.
    # Looks like there are two deep turns to left and many different turns to right.
    # Looks like nothing less than 3 forward is not that goo
