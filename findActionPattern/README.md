### Short term actions into patterns. Finding best patterns.

Accessible calculation capacity is very limited. It would be impossible to receive 20 observations (iterations) a second and take deliberate action and train model on each time step. One solution is to take less than 20 actions a second. Out of all possible patterns of actions there are probably few patterns that are better than other. Finding best patterns is what is done here. Taking random action every five iterations. Random actions belongs to a pattern of five actions. Reward is saved with pattern name and number of times patter was called. At the end, we should be able to select best 5 patterns. These patterns are possible actions in later phases. Now instead of dealing with new observation, decision making, and model training 20 times a second, it is done less than once per second.

To repeat, run following command (on root directory)
```sh
python runActionPatterns.py
```

Best patters are:
```
[
  [forward, forward, left and forward, left and forward, left and forward],
  [forward, forward, forward, left and forward, left],
  [forward, forward, forward, forward, left and forward],
  [forward, forward, forward, forward, right and forward],
  [forward, forward, right and forward, right and forward, right and forward]
]
```

[Here](https://gitlab.com/tjnikkila/game-ai/blob/master/findActionPattern/analysis/README.md) is some analysis of logic

#### Note:

One goal was also test how hard it would be to find perfect trajectory to play this particular game. Architecture of this solution should support finding best patterns on top of patterns. Finding perfect trajectory was not so exiting so that plan was skipped.
