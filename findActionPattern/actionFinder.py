import gym
import universe
import sys


from findActionPattern.PlayerAi import PlayerAi

class ActionFinder:
    def run(self, patternsTo, rewardSystem):
        env = gym.make('flashgames.CoasterRacer-v0')
        env.configure(remotes=1)
        observation_n = env.reset()
        driver =  PlayerAi(rewardSystem, patternsTo)
        event = driver.defaultEvent()

        while True:
            action_n = [event for ob in observation_n]
            observation_n, reward_n, done_n, info = env.step(action_n)
            if(observation_n[0] != None):
                event = driver.render(reward_n)

            env.render()

# if __name__ == '__main__':
#     main()
