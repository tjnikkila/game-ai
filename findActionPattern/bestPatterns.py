left = [('KeyEvent', 'ArrowUp', False),('KeyEvent', 'ArrowLeft', True), ('KeyEvent', 'ArrowRight', False)]
right = [('KeyEvent', 'ArrowUp', False),('KeyEvent', 'ArrowLeft', False), ('KeyEvent', 'ArrowRight', True)]
leftForward = [('KeyEvent', 'ArrowUp', True) ,('KeyEvent', 'ArrowLeft', True), ('KeyEvent', 'ArrowRight', False)]
rightForward = [('KeyEvent', 'ArrowUp', True) ,('KeyEvent', 'ArrowLeft', False), ('KeyEvent', 'ArrowRight', True)]
forward = [('KeyEvent', 'ArrowUp', True) ,('KeyEvent', 'ArrowLeft', False), ('KeyEvent', 'ArrowRight', False)]

# 5 best actions baed on the figure 1 in /findActionPattern/analysis/README.md
# change move every 5 renders. There are 5 moves per pattern
# Patterns are:
# NB: changing this order will break code elsewhere
patterns = [[forward, forward, leftForward, leftForward, leftForward],
            [forward, forward, forward, leftForward, left],
            [forward, forward, forward, forward, leftForward],
            [forward, forward, forward, forward, rightForward],
            [forward, forward, rightForward, rightForward, rightForward]]

balanced_patterns = [[forward, forward, leftForward, leftForward, leftForward],
            [forward, forward, forward, forward, leftForward],
            [forward, forward, forward, forward, forward],
            [forward, forward, forward, forward, rightForward],
            [forward, forward, rightForward, rightForward, rightForward]]
