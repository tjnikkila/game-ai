import random
import numpy as np
from universe.vncdriver import constants
from findActionPattern.reward.PatternReward import PatternReward
from findActionPattern.reward.DeferredPatternReward import DeferredPatternReward
from findActionPattern.similarity import CalculateSimilarity
from findActionPattern.ActionEngine import ActionEngineInterface, ActionState

class PlayerAi:
    def createRewardSystem(self, rewardSystem, patternsTo):
        folderToSavePatterns = "findActionPattern/patterns"
        if patternsTo is not None:
            folderToSavePatterns = patternsTo

        similarity = CalculateSimilarity()
        if rewardSystem is None or rewardSystem == "basic":
            return PatternReward(similarity, folderToSavePatterns)
        elif rewardSystem == "deferred":
            return DeferredPatternReward(similarity, folderToSavePatterns)
        else:
            raise Exception("Reward system does not exists. Reward system name " + rewardSystem)

    def __init__(self, rewardSystem, patternsTo):
        patternRewards = self.createRewardSystem(rewardSystem, patternsTo)
        self.actionEngine = ActionEngineInterface(5, 5, 20000, patternRewards)
        patternId = self.actionEngine.getRandomPatternId()
        self.actionEngine.startNewPattern(patternId)

    def defaultEvent(self):
        return self.actionEngine.initialAction()

    def render(self, rewards):
        state = self.actionEngine.render(rewards)
        if(state['actionState'] == ActionState.ended):
            patternId = self.actionEngine.getNextPatternId()
            self.actionEngine.startNewPattern(patternId)
        return state['action']
