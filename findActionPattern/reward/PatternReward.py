from findActionPattern.reward.PatternLoader import PatternLoader
from findActionPattern.reward.rewardFunctions import newPatternToReward, mergePatterns, createUpdatedPattern

class PatternReward:
    rewardedPatterns = {}
    patternToReward = None

    def __init__(self, similarity, patternsFolderPath):
        self.similarity = similarity
        self.patternLoader = PatternLoader(patternsFolderPath, "basicPatterns")
        self.rewardedPatterns = self.patternLoader.loadPatterns()

    def startPattern(self, patternId):
        if(self.patternToReward is not None ):
            updatedPattern = createUpdatedPattern(self.patternToReward, self.rewardedPatterns, self.similarity)
            self.rewardedPatterns[updatedPattern['patternId']] = updatedPattern

        self.patternToReward = newPatternToReward(patternId)

    def render(self, reward):
        self.patternToReward['reward'] = self.patternToReward['reward'] + reward
        return self.patternToReward['reward']

    def getRewardedPatterns(self):
        return list(self.rewardedPatterns.keys())

    def savePatterns(self):
        self.patternLoader.savePatterns(self.rewardedPatterns)
        #print("total renders", sum(pattern["count"] + pattern["nonRewardedCount"] for pattern in list(self.rewardedPatterns.values())))

"""
import sys
sys.path.insert(0, '/Users/timo.nikkila/projects/ml/game-ai/findActionPattern')
from similarity import CalculateSimilarity
similarity = CalculateSimilarity()
f = PatternReward(similarity, "removeMe")
f.startPattern("fffff")
print(f.render(1))
print(f.render(1))
print(f.render(1))
f.startPattern("fffll")
print(f.render(1))
f.savePatterns()
"""
