from findActionPattern.reward.rewardFunctions import newPatternToReward, mergePatterns, createUpdatedPattern
import queue
from findActionPattern.reward.PatternLoader import PatternLoader

# pistä patternId:tä Queryyn. Jos joku saa rewardsin niin lisää rewards kaikille queryssä ja tyhjennä query oikeista arvoista ja täytä deferred arvolla
# kun poistat täyteen tulleen queryn lopusta niin merkkaa positetulle patternId:lle antiCount

class DeferredPatternReward:
    rewardedPatterns = {}
    patternDeferred = "deferred"
    patternToReward = None
    patternHistoryLength = 5

    def __init__(self, similarity, patternsFolderPath):
        self.similarity = similarity
        self.patternLoader = PatternLoader(patternsFolderPath, "secDeferred")
        self.rewardedPatterns = self.patternLoader.loadPatterns()

        self.patternQueue = self.fullWithDeferred(queue.Queue())

    def fullWithDeferred(self, patternQueue):
        for i in range(self.patternHistoryLength):
            patternQueue.put(self.patternDeferred)
        return patternQueue

    def startPattern(self, patternId):
        if(self.patternToReward is not None ):
            self.patternQueue.put(self.patternToReward)
            if (self.patternToReward['reward'] > 0):
                while not self.patternQueue.empty():
                    pattern = self.patternQueue.get()
                    if(pattern != self.patternDeferred ):
                        pattern['reward'] = self.patternToReward['reward']
                        updatedPattern = createUpdatedPattern(pattern, self.rewardedPatterns, self.similarity)
                        self.rewardedPatterns[updatedPattern['patternId']] = updatedPattern
                self.patternQueue = self.fullWithDeferred(self.patternQueue)
            else:
                pattern = self.patternQueue.get()
                if(pattern != self.patternDeferred ):
                    updatedPattern = createUpdatedPattern(pattern, self.rewardedPatterns, self.similarity)
                    self.rewardedPatterns[updatedPattern['patternId']] = updatedPattern



        self.patternToReward = newPatternToReward(patternId)

    def render(self, reward):
        self.patternToReward['reward'] = self.patternToReward['reward'] + reward
        return self.patternToReward['reward']

    def getRewardedPatterns(self):
        return list(self.rewardedPatterns.keys())

    def savePatterns(self):
        self.patternLoader.savePatterns(self.rewardedPatterns)
        print("total renders", sum(pattern["count"] + pattern["nonRewardedCount"] for pattern in list(self.rewardedPatterns.values())))
"""
import sys
sys.path.insert(0, '/Users/timo.nikkila/projects/ml/game-ai/findActionPattern')
from similarity import CalculateSimilarity
similarity = CalculateSimilarity()
f = DeferredPatternReward(similarity, "removeMe")
f.startPattern("fffff")
print(f.render(1))
print(f.render(1))
print(f.render(1))
f.startPattern("fffll")
print(f.render(1))
#f.savePatterns()
"""
