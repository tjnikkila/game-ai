def mergePatterns(oldPattern, patternToAdd):
    def addToCounts(pat):
        if pat['reward'] > 0:
            pat['count'] = 1
            pat['nonRewardedCount'] = 0
            return pat
        else:
            pat['count'] = 0
            pat['nonRewardedCount'] = 1
            return pat

    if oldPattern is None:
        return addToCounts(patternToAdd)
    elif patternToAdd is None:
        return oldPattern
    else:
        if oldPattern['patternId'] != patternToAdd['patternId']:
            print("Converting patternId " + patternToAdd['patternId'] + " to patternId " + oldPattern['patternId'] )
        patternToAdd = addToCounts(patternToAdd)
        return {
            'reward': oldPattern['reward'] + patternToAdd['reward'],
            'patternId': oldPattern['patternId'],
            'count': oldPattern['count'] + patternToAdd['count'],
            'nonRewardedCount': oldPattern['nonRewardedCount'] + patternToAdd['nonRewardedCount']
        }

def newPatternToReward(patternId):
    return { 'reward': 0, \
        'patternId': patternId,
        'count': 0 ,
        'nonRewardedCount': 0}

def createUpdatedPattern(newPattern, rewardedPatterns, similarity):
    localized = similarity.localizePatternId(newPattern['patternId'])
    similar = similarity.getSimilar(rewardedPatterns, localized)
    return mergePatterns(similar, newPattern)
