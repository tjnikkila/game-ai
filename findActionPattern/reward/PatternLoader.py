from os import listdir
from os.path import isfile, join
import os
from datetime import datetime
import csv
import json
import ast
from pprint import pprint

class PatternLoader:
    def __init__(self, patternsFolderPath, namePattern):
        self.patternsFolderPath = patternsFolderPath
        self.namePattern = namePattern

    def patternFileName(self):
        filesInFolder = [f for f in listdir(self.patternsFolderPath) if isfile(join(self.patternsFolderPath, f))]
        def isFileOfInterest(fileName):
            return fileName.startswith(self.namePattern)
        fil2 = filter(isFileOfInterest, filesInFolder)
        def toNameDate(fileName):
            datePart = fileName[len(self.namePattern):]
            dateStr = os.path.splitext(datePart)[0]
            try:
                return {'fileName': fileName,'dateObj':datetime.strptime(dateStr, '%Y-%m-%dT%H:%M:%SZ')}
            except Exception:
                return

        def isDefined(obj):
            return obj != None

        fil3 = map(toNameDate, fil2)
        fil4 = filter(isDefined, fil3)
        def getDate(fileNameDate):
            return fileNameDate['dateObj']
        fil5 = sorted(fil4, key= getDate)

        if len(fil5) > 0:
            return fil5[-1:][0]["fileName"]
        else:
            return None

    def loadFilePatterns(self, fileName):
        with open(self.patternsFolderPath + "/" + fileName) as data_file:
            fileStr = data_file.read()
            datapoints = ast.literal_eval(fileStr)
            return datapoints

    def loadPatterns(self):
        fileName = self.patternFileName()
        if fileName is None:
            return {}
        else:
            return self.loadFilePatterns(fileName)

    def savePatterns(self, patterns):
        if patterns != {}:
            jsonStr = json.dumps(patterns)
            fileName = self.namePattern + datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
            with open(self.patternsFolderPath + "/" + fileName + ".json","w") as patternWriter:
                patternWriter.write(jsonStr)



#u = PatternLoader("/path/findActionPattern/bestPatterns", "bestPatterns")
#u.loadPatterns().keys()
# print(u.loadPatterns())
#jsond = json.dumps({'aa': {'count': 2, 'rewards': 31, 'patternId': 'aa'}, 'ax': {'count': 4, 'rewards': 34, 'patternId': 'ax'}, 'ab': {'count': 1, 'rewards': 34, 'patternId': 'ab'}})
#jsonDum = json.dumps({'aa': {'count': 2, 'rewards': 31, 'patternId': 'aa'}, 'ax': {'count': 4, 'rewards': 34, 'patternId': 'ax'}, 'ab': {'count': 1, 'rewards': 34, 'patternId': 'ab'})
#u.savePatterns(jsond)
