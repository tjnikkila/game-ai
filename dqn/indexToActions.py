from findActionPattern.bestPatterns import balanced_patterns, forward, patterns
import random
class IndexToActions:
    def toActions(self, index):
        return balanced_patterns[index]

    def random(self):
        return balanced_patterns[random.randint(0,4)]

    def forwards(self):
        return [forward]*5
