import numpy as np

class PreProcessor:
    # down_scale = from util.observationFunctions import getFocusedDownScaledBWImage()
    def __init__(self, history_length, down_scale):
        self.history_length = history_length
        self.history = None
        self.down_scale = down_scale

    def process(self, observation):
        down_scaled = self.down_scale(observation)
        if self.history is None:
            self.history = np.array([down_scaled])
        else:
            if self.history.shape[0] == self.history_length:
                self.history = np.roll(self.history,1, axis=0)
                self.history[0] = down_scaled
            else:
                self.history = np.append([down_scaled] ,self.history, axis=0)

        if self.history.shape[0] < self.history_length:
            return None
        else:
            #[np.dstack((getFocusedDownScaledImage(observations1, True), getFocusedDownScaledImage(observations2, True), getFocusedDownScaledImage(observations3, True), getFocusedDownScaledImage(observations4, True)))]
            return np.dstack(tuple(self.history))
