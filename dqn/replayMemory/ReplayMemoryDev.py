from util.devObservations import DevObservations
from util.envEmulator import EnvEmulator
import numpy as np
from util.observationFunctions import getFocusedDownScaledImage

class ReplayMemorySameSample:
    def __init__(self):
        metaData = DevObservations().getObservationMetaData(0,3)
        env = EnvEmulator(metaData)
        observations1, rewards1, done_n, info = env.step(None)
        observations2, rewards2, done_n, info = env.step(None)
        observations3, rewards3, done_n, info = env.step(None)
        observations4, rewards4, done_n, info = env.step(None)
        meta = env.getCurrentMeta()
        #actionIndex4 = meta["actionIndex"]
        patternIndex4 = meta["patternIndex"]
        observations5, rewards5, done_n, info = env.step(None)
        preprocessedT = [np.dstack((getFocusedDownScaledImage(observations1, True), getFocusedDownScaledImage(observations2, True), getFocusedDownScaledImage(observations3, True), getFocusedDownScaledImage(observations4, True)))]
        preprocessedTPlus1 = [np.dstack((getFocusedDownScaledImage(observations2, True), getFocusedDownScaledImage(observations3, True), getFocusedDownScaledImage(observations4, True), getFocusedDownScaledImage(observations5, True)))]

        meta2 = env.getCurrentMeta()
        patternIndex5 = meta2["patternIndex"]
        observations6, rewards6, done_n, info = env.step(None)

        preprocessedTPlus2 = [np.dstack((getFocusedDownScaledImage(observations3, True), getFocusedDownScaledImage(observations4, True), getFocusedDownScaledImage(observations5, True), getFocusedDownScaledImage(observations6, True)))]

        # phi, a,r,phi_t_+_1
        #self.samples = [(preprocessedT, patternIndex4, 0.2, preprocessedTPlus1)]
        #self.samples = [(preprocessedT, patternIndex4, 0.2, preprocessedTPlus1),(preprocessedTPlus1, patternIndex5, 0.1, preprocessedTPlus2)]

        self.phi0s = preprocessedT + preprocessedTPlus1
        self.indexes = [patternIndex4, patternIndex5]
        self.rewards = [0.2,0.1]
        self.phi1s = preprocessedTPlus1 + preprocessedTPlus2

        """
        phi_plus1 = np.array([x[3] for x in sample])
        phi_plus1 = np.squeeze(phi_plus1, axis=1)

        phi = np.array([x[0] for x in sample])
        phi = np.squeeze(phi, axis=1)

        reward = np.array([x[2] for x in sample])
        action = np.array([x[1] for x in sample])
        """

    def get_sample(self):
        """
        phi1 = np.array([x[3] for x in self.samples])
        phi1 = np.squeeze(phi1, axis=1)

        phi0 = np.array([x[0] for x in self.samples])
        phi0 = np.squeeze(phi0, axis=1)

        reward = np.array([x[2] for x in self.samples])
        action = np.array([x[1] for x in self.samples])
        """

        return self.phi0s, self.indexes, self.rewards, self.phi1s
        # return {'sample': self.samples, 'phi0':phi0, 'phi1':phi1,'reward':reward,'action':action}
