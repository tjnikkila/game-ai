import random

class ReplayMemory:
    def __init__(self, transitions_max = 1000, batch_size = 1, zero_rule = False):
        # TODO: deprecate
        #self.transitions = None
        self.transitions_max = transitions_max

        self.batch_size = batch_size
        self.phi0s = []
        self.indexes = []
        self.rewards = []
        self.phi1s = []
        self.zero_rule = zero_rule

    def add_transition(self, phi0, index, reward, phi1):
        if self.phi0s is None:
            self.phi0s = [phi0]
            self.indexes = [index]
            self.rewards = [reward]
            self.phi1s = [phi1]
        elif len(self.phi0s) < self.transitions_max:
            self.phi0s.append(phi0)
            self.indexes.append(index)
            self.rewards.append(reward)
            self.phi1s.append(phi1)
        else:
            if self.zero_rule and reward == 0:
                return
            replay_index = random.randint(0,self.transitions_max - 1)
            self.phi0s[replay_index] = phi0
            self.indexes[replay_index] = index
            self.rewards[replay_index] = reward
            self.phi1s[replay_index] = phi1


    def get_sample(self):
        if self.phi0s is None:
            return None, None, None, None
        elif len(self.phi0s) == self.batch_size:
            return self.phi0s, self.indexes, self.rewards, self.phi1s
        elif len(self.phi0s) > self.batch_size:
            from_index = random.randint(0, len(self.phi0s) - 1 - self.batch_size)
            to = from_index+self.batch_size
            return self.phi0s[from_index:to], self.indexes[from_index:to], self.rewards[from_index:to], self.phi1s[from_index:to]
        else:
            return None, None, None, None
