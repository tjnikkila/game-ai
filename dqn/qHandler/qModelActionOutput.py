import tensorflow as tf
import sonnet as snt
from pprint import pprint


class QModelActionOutput:
    def __init__(self, *, outputSize):
        self.variables = {}
        self.outputSize = outputSize

    def __call__(self, inputs, is_training, keep_prob):
        return self.q_model(inputs, is_training, keep_prob)

    def assign(self, copyFrom):
        assigns = {}
        for key in copyFrom.variables.keys():
            assigns[key] = self.variables[key].assign(copyFrom.variables[key])

        return assigns

    def q_model(self, inputs, is_training, keep_prob):
        """A custom build method to wrap into a sonnet Module."""
        initializers={"w": tf.truncated_normal_initializer(stddev=0.1),
              "b": tf.truncated_normal_initializer(stddev=0.1)}

        #batch_norm_init={"gamma": tf.truncated_normal_initializer(stddev=0.4),
        #      "beta": tf.truncated_normal_initializer(stddev=0.4)}

        with tf.name_scope("first_conv1"):
            # data_format = "NCHW" is not supported by CPU
            conv2dmod1 = snt.Conv2D(output_channels=32, kernel_shape=9, \
                stride=4, name="conv_2d_1", padding="VALID", initializers=initializers)
            # oooo (?, 140, 200, 12)
            #print("oooo", inputs.get_shape())
            outputs = conv2dmod1(inputs)
            outputs = snt.BatchNorm()(outputs, is_training=is_training)
            # pppp (?, 33, 48, 32)
            # print("pppp", outputs.get_shape())
            #outputs = tf.layers.max_pooling2d(inputs=outputs, pool_size=[2,2], strides=2)
            outputs = tf.layers.max_pooling2d(inputs=outputs, pool_size=[2,2], strides=2)
            outputs = tf.nn.relu(outputs)
            outputs = tf.nn.dropout(outputs, keep_prob=keep_prob)

            if is_training:
                # for tensorboard
                tf.summary.histogram("weights", conv2dmod1.w)
                tf.summary.histogram("biases", conv2dmod1.b)
                tf.summary.histogram("activation", outputs)

        with tf.name_scope("second_conv2"):
            conv2dmod2 = snt.Conv2D(output_channels=64, kernel_shape=7, stride=3, name="conv_2d_2", padding="VALID", initializers=initializers)
            outputs = conv2dmod2(outputs)
            outputs = snt.BatchNorm()(outputs, is_training=is_training)
            # eeeee (?, 9, 14, 64)
            #print("eeeee", outputs.get_shape())
            #outputs = tf.layers.max_pooling2d(inputs=outputs, pool_size=[2,2], strides=2)
            # eeeeeaaa (?, 4, 7, 64)
            #print("eeeeeaaa", outputs.get_shape())

            outputs = tf.nn.relu(outputs)
            if is_training:
                # for tensorboard
                tf.summary.histogram("weights", conv2dmod2.w)
                tf.summary.histogram("biases", conv2dmod2.b)
                tf.summary.histogram("activation", outputs)

        with tf.name_scope("third_conv2"):
            conv2dmod3 = snt.Conv2D(output_channels=64, kernel_shape=[4,6], stride=4, name="conv_2d_3", padding="VALID", initializers=initializers)
            outputs = conv2dmod3(outputs)
            # qqqq (?, 1, 1, 64)
            #outputs = snt.BatchNorm()(outputs, is_training=is_training)
            #outputs = tf.layers.max_pooling2d(inputs=outputs, pool_size=[2,2], strides=2)
            print("qqqq", outputs.get_shape())
            outputs = tf.nn.relu(outputs)

            if is_training:
                # for tensorboard
                tf.summary.histogram("weights", conv2dmod3.w)
                tf.summary.histogram("biases", conv2dmod3.b)
                tf.summary.histogram("activation", outputs)

        """
        with tf.name_scope("fourt_conv2"):
            # stride = [3,5]
            # kernel_shape = [2,4]
            # padding: `snt.VALID`
            conv2dmod4 = snt.Conv2D(output_channels=64, kernel_shape=9, stride=4, name="conv_2d_4")
            outputs = conv2dmod4(outputs)
            # (?, 9, 13, 64)
            # max poolilla  (?, 5, 6, 64)
            #print("tttt", outputs.get_shape())
            outputs = snt.BatchNorm()(outputs, is_training=is_training)
            # (?, 1, 1, 64)
            print("yyyy", outputs.get_shape())
            outputs = tf.nn.relu(outputs)
            if is_training:
                # for tensorboard
                tf.summary.histogram("weights", conv2dmod4.w)
                tf.summary.histogram("biases", conv2dmod4.b)
                tf.summary.histogram("activation", outputs)
        """

        # TODO: TEkeeko batchFlatten flattayksen?
        outputs = snt.BatchFlatten()(outputs)

        outputs = tf.nn.dropout(outputs, keep_prob=keep_prob)

        linearToOut = snt.Linear(output_size=64, initializers=initializers)
        outputs = linearToOut(outputs)
        #outputs = snt.BatchNorm()(outputs, is_training=is_training)
        outputs = tf.nn.relu(outputs)

        # llll (?, 64)
        #print("llll", outputs.get_shape())

        linearToOut2 = snt.Linear(output_size=self.outputSize, initializers=initializers)
        outputs = linearToOut2(outputs)
        # llluu (?, 5)
        # print("llluu", outputs.get_shape())

        # for assigning one model parameters to another model
        self.variables["conv2dmod1_w"] = conv2dmod1.w
        self.variables["conv2dmod1_b"] = conv2dmod1.b
        self.variables["conv2dmod2_w"] = conv2dmod2.w
        self.variables["conv2dmod2_b"] = conv2dmod2.b
        self.variables["conv2dmod3_w"] = conv2dmod3.w
        self.variables["conv2dmod3_b"] = conv2dmod3.b
        #self.variables["conv2dmod4_w"] = conv2dmod4.w
        #self.variables["conv2dmod4_b"] = conv2dmod4.b
        self.variables["linearToOut_w"] = linearToOut.w
        self.variables["linearToOut_b"] = linearToOut.b
        self.variables["linearToOut2_w"] = linearToOut2.w
        self.variables["linearToOut2_b"] = linearToOut2.b

        #outputs = tf.nn.softmax(outputs)
        return outputs
