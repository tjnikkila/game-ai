import tensorflow as tf
import sonnet as snt
from pprint import pprint


class QModelActionInput:
    def __init__(self, *, outputSize):
        self.variables = {}
        self.outputSize = outputSize

    def __call__(self, inputs, is_training, keep_prob):
        return self.q_model(inputs, is_training, keep_prob)

    def assign(self, copyFrom):
        assigns = {}
        for key in copyFrom.variables.keys():
            assigns[key] = self.variables[key].assign(copyFrom.variables[key])

        return assigns

    def q_model(self, inputs, action, is_training, keep_prob):
        """A custom build method to wrap into a sonnet Module."""
        with tf.name_scope("first_conv1"):
            # data_format = "NCHW" is not supported by CPU
            conv2dmod1 = snt.Conv2D(output_channels=32, kernel_shape=5, stride=2, name="conv_2d_1")
            outputs = conv2dmod1(inputs)
            outputs = snt.BatchNorm()(outputs, is_training=is_training)
            outputs = tf.nn.relu(outputs)
            if is_training:
                # for tensorboard
                tf.summary.histogram("weights", conv2dmod1.w)
                tf.summary.histogram("biases", conv2dmod1.b)
                tf.summary.histogram("activation", outputs)

        with tf.name_scope("second_conv2"):
            conv2dmod2 = snt.Conv2D(output_channels=64, kernel_shape=5, stride=2, name="conv_2d_2")
            outputs = conv2dmod2(outputs)
            outputs = snt.BatchNorm()(outputs, is_training=is_training)
            outputs = tf.nn.relu(outputs)
            if is_training:
                # for tensorboard
                tf.summary.histogram("weights", conv2dmod2.w)
                tf.summary.histogram("biases", conv2dmod2.b)
                tf.summary.histogram("activation", outputs)

        with tf.name_scope("third_conv2"):
            conv2dmod3 = snt.Conv2D(output_channels=64, kernel_shape=5, stride=2, name="conv_2d_3")
            outputs = conv2dmod3(outputs)
            outputs = snt.BatchNorm()(outputs, is_training=is_training)
            outputs = tf.nn.relu(outputs)
            if is_training:
                # for tensorboard
                tf.summary.histogram("weights", conv2dmod3.w)
                tf.summary.histogram("biases", conv2dmod3.b)
                tf.summary.histogram("activation", outputs)

        outputs = snt.BatchFlatten()(outputs)
        if is_training:
            outputs = tf.nn.dropout(outputs, keep_prob=keep_prob)

        linearToOut = snt.Linear(output_size=50)
        outputs = linearToOut(outputs)

        # TODO: add this weights to variables
        action_in = snt.Linear(output_size=5)
        action_out = action_in(action)

        linearToOut2 = snt.Linear(output_size=30)
        outputs = linearToOut2(tf.concat([outputs, action_out], axis=1))

        if is_training:
            outputs = tf.nn.dropout(outputs, keep_prob=keep_prob)

        linearToOut3 = snt.Linear(output_size=self.outputSize)
        outputs = linearToOut3(outputs)

        # for assigning one model parameters to another model
        self.variables["conv2dmod1_w"] = conv2dmod1.w
        self.variables["conv2dmod1_b"] = conv2dmod1.b
        self.variables["conv2dmod2_w"] = conv2dmod2.w
        self.variables["conv2dmod2_b"] = conv2dmod2.b
        self.variables["conv2dmod3_w"] = conv2dmod3.w
        self.variables["conv2dmod3_b"] = conv2dmod3.b
        self.variables["linearToOut_w"] = linearToOut.w
        self.variables["linearToOut_b"] = linearToOut.b
        self.variables["linearToOut2_w"] = linearToOut2.w
        self.variables["linearToOut2_b"] = linearToOut2.b
        self.variables["linearToOut3_w"] = linearToOut3.w
        self.variables["linearToOut3_b"] = linearToOut3.b

        #outputs = tf.nn.softmax(outputs)
        return outputs
