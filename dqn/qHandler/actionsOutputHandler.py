import tensorflow as tf
import sonnet as snt
from pprint import pprint
import numpy as np
from dqn.qHandler.qModelActionOutput import QModelActionOutput
import random
from os.path import isfile

class ActionsOutputHandler:
    def __init__(self, sess, inputShape, batchSize, replayMemory, *, \
        reset_interval, explorationProbability, gamma, learning_rate=1e-8, \
        keep_prob = 0.8, model_params_path = None, is_training=True):

        self.inputData = tf.placeholder(tf.float32, shape=inputShape, name="x1")
        self.inputDataTarget = tf.placeholder(tf.float32, shape=inputShape, name="x2")
        self.sess = sess
        self.replayReward = tf.placeholder(tf.float32, shape=(batchSize), name="x3")
        self.replayAction = tf.placeholder(tf.int32, shape=(batchSize), name="x4")

        self.outputSize = 5
        qModel = QModelActionOutput(outputSize = self.outputSize)
        self.model = snt.Module(qModel.q_model, name='q_model')(self.inputData, is_training=is_training,keep_prob=tf.constant(1.0))
        qModelTarget = QModelActionOutput(outputSize = self.outputSize)
        self.modelTarget = snt.Module(qModelTarget.q_model, name='q_model_target')(self.inputDataTarget, is_training=is_training,keep_prob=tf.constant(1.0))

        self.dqnWeights = {'qModel': qModel, 'qModelTarget': qModelTarget}

        self.explorationProbability = explorationProbability
        self.replayMemory = replayMemory
        self.gamma = gamma
        self.learning_rate = learning_rate
        self.loss = None
        self.train_step = None
        self.maxQ_target = None
        self.saver = tf.train.Saver()
        self.model_params_path = model_params_path

        self.reset_interval = reset_interval
        self.optimization_count = 0
        self.target_reseted_count = 0

        np.set_printoptions(precision=10)

    def reset_Q_target(self):
        q_model_target = self.dqnWeights['qModelTarget']
        q_model =  self.dqnWeights['qModel']
        combine_param_values = q_model_target.assign(q_model)
        self.sess.run([combine_param_values])

    def setUp(self):
        #self.train_step = tf.train.GradientDescentOptimizer(learning_rate=self.learning_rate, name="optimizer").minimize(self.getLoss())
        #self.train_step = tf.train.AdamOptimizer(learning_rate=self.learning_rate, name="optimizer").minimize(self.getLoss())
        #self.optim = tf.train.RMSPropOptimizer(self.learning_rate_op, momentum=0.95, epsilon=0.01).minimize(self.loss)
        self.train_step = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate, momentum=0.95, epsilon=0.01).minimize(self.getLoss())
        #tf.initialize_all_variables().run()

        self.sess.run(tf.global_variables_initializer())
        if self.model_params_path is not None and isfile(self.model_params_path + ".ckpt" + ".meta"):
            self.saver.restore(self.sess, self.model_params_path + ".ckpt")

        self.reset_Q_target()

    def nextIndex(self, phi):
        if random.uniform(0, 1) < self.explorationProbability:
            return random.randint(0, self.outputSize - 1)
        else:
            input_v = np.array([phi])
            actionValues = self.sess.run(self.model, feed_dict = {self.inputData: input_v})
            np.set_printoptions(precision=10)
            print(actionValues, "action values ")
            ab =  np.argmax(actionValues, axis=1)[0]
            return ab

    def getQTargetMax(self):
        if self.maxQ_target is None:
            self.maxQ_target = tf.reduce_max(self.modelTarget, axis=1)

        return self.maxQ_target

    def getLoss(self):
        if self.loss is None:
            self.targetValues = tf.placeholder(tf.float32, shape=(None), name="y_j")
            indexRange = tf.range(tf.shape(self.replayAction)[0])
            maxValuesIndexes = tf.stack((indexRange, self.replayAction), -1)
            modelValues = tf.gather_nd(self.model, maxValuesIndexes)
            self.maxValuesIndexes = maxValuesIndexes
            self.loss = tf.pow(tf.subtract(self.targetValues, modelValues), 2)

        return self.loss

    def optimize(self):
        if self.optimization_count % self.reset_interval == 0:
            self.target_reseted_count += 1
            print("Q target is updated. " + str(self.target_reseted_count))
            self.reset_Q_target()

        self.optimization_count += 1
        phi0s, indexes, rewards, phi1s = self.replayMemory.get_sample()

        if phi0s is not None:
            qTargetValues = self.sess.run(self.getQTargetMax(), feed_dict = {self.inputDataTarget: phi1s})
            y_j = rewards + self.gamma * qTargetValues

            trainStep, loss = self.sess.run([self.train_step, self.loss], feed_dict = {self.targetValues: y_j, self.inputData: phi0s, self.replayAction: indexes})
            return loss
        else:
            return None


    def save(self):
        # TODO: saving also target, which is unnecessary.
        if self.model_params_path is not None:
            self.saver.save(self.sess, self.model_params_path + ".ckpt")
        else:
            print("Model not saved since target directory is not given.")
