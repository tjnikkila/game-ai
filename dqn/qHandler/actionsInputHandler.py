import tensorflow as tf
import sonnet as snt
from pprint import pprint
import numpy as np
from dqn.qHandler.qModelActionInput import QModelActionInput
import random
from os.path import isfile
import pandas as pd

class ActionsInputHandler:
    def __init__(self, sess, inputShape, batchSize, replayMemory, *, \
        reset_interval, explorationProbability, gamma, learning_rate=1e-8, \
        keep_prob = 0.5, model_params_path = None, is_training=True):

        self.outputSize = 1
        self.number_of_actions = 5
        self.input_shape = inputShape
        self.inputData = tf.placeholder(tf.float32, shape=inputShape, name="x1")
        # TODO: these two lines should be possible to combine
        self.action = tf.placeholder(tf.float32, shape=(None, self.number_of_actions), name="action")
        self.action_target = tf.placeholder(tf.float32, shape=(None, self.number_of_actions), name="action_target")
        self.inputDataTarget = tf.placeholder(tf.float32, shape=inputShape, name="x2")
        self.sess = sess
        self.replayReward = tf.placeholder(tf.float32, shape=(None), name="x3")

        self.batch_size = batchSize

        qModel = QModelActionInput(outputSize = self.outputSize)
        # https://github.com/deepmind/sonnet/blob/master/sonnet/examples/module_with_build_args.py
        
        self.model = snt.Module(qModel.q_model, name='q_model')(self.inputData, self.action, \
            is_training=is_training,keep_prob=tf.constant(1.0))
        qModelTarget = QModelActionInput(outputSize = self.outputSize)
        self.modelTarget = snt.Module(qModelTarget.q_model, name='q_model_target')(self.inputDataTarget, \
            self.action_target, is_training=is_training,keep_prob=tf.constant(1.0))

        self.dqnWeights = {'qModel': qModel, 'qModelTarget': qModelTarget}

        self.explorationProbability = explorationProbability
        self.replayMemory = replayMemory
        self.gamma = gamma
        self.learning_rate = learning_rate
        self.loss = None
        self.train_step = None
        self.maxQ_target = None
        self.saver = tf.train.Saver()
        self.model_params_path = model_params_path

        self.reset_interval = reset_interval
        self.optimization_count = 0
        self.target_reseted_count = 0

    def reset_Q_target(self):
        q_model_target = self.dqnWeights['qModelTarget']
        q_model =  self.dqnWeights['qModel']
        combine_param_values = q_model_target.assign(q_model)
        self.sess.run([combine_param_values])

    def setUp(self):
        self.sess.run(tf.global_variables_initializer())
        if self.model_params_path is not None and isfile(self.model_params_path + ".ckpt" + ".meta"):
            self.saver.restore(self.sess, self.model_params_path + ".ckpt")

        self.reset_Q_target()

    def nextIndex(self, phi):
        # TODO: explorationProbability should diminish
        if random.uniform(0, 1) < self.explorationProbability:
            print("randome act")
            return random.randint(0, self.number_of_actions - 1)
        else:
            action_inputs = self.all_actions_one_hot_batch(1)
            batched_input = self.phi_for_all_actions_batch(phi, 1)

            actionValues = self.sess.run([self.model], \
                feed_dict = {self.inputData: batched_input, self.action: action_inputs})

            actionValues = self.decode_model_output(actionValues, 1)
            actionValues = np.squeeze(np.argmax(actionValues, axis=1))

            return actionValues

    def getLoss(self):
        if self.loss is None:
            self.targetValues = tf.placeholder(tf.float32, shape=(None), name="y_j")
            self.loss = tf.pow(tf.subtract(self.targetValues, self.model), 2)

        return self.loss

    def all_actions_one_hot_batch(self, batch_size):
        # Predicted value has to be calculater for all possible actions in order
        # to take highest value. So one hot must be made for all actions
        action_inputs = pd.get_dummies(pd.Series(range(self.number_of_actions))).values.astype(np.float32)
        # reshape to run all actions of all batches in one batch.
        return np.reshape([action_inputs]*batch_size,(batch_size*self.number_of_actions,self.number_of_actions))

    def phi_for_all_actions_batch(self, phis, batch_size):
        batched_input = np.array([phis] * self.number_of_actions)
        return np.reshape(batched_input, (batch_size*self.number_of_actions,self.input_shape[1], self.input_shape[2], self.input_shape[3]))

    def decode_model_output(self, out, batch_size):
        return np.reshape(out, (batch_size, self.number_of_actions, self.outputSize))
    def optimize(self):
        if self.optimization_count % self.reset_interval == 0:
            self.target_reseted_count += 1
            print("Q target is updated. " + str(self.target_reseted_count))
            self.reset_Q_target()

        self.optimization_count += 1
        phi0s, indexes, rewards, phi1s = self.replayMemory.get_sample()

        if phi0s is not None:
            action_inputs = self.all_actions_one_hot_batch(self.batch_size)
            batched_input = self.phi_for_all_actions_batch(phi1s, self.batch_size)

            qTargetValues = self.sess.run(self.modelTarget, feed_dict = { self.inputDataTarget: batched_input, \
                self.action_target: action_inputs })

            qTargetValues = self.decode_model_output(qTargetValues, self.batch_size)
            qTargetValues = np.squeeze(np.amax(qTargetValues, axis=1))
            y_j = rewards + self.gamma * qTargetValues

            if self.train_step is None:
                self.train_step = tf.train.GradientDescentOptimizer(learning_rate=self.learning_rate, name="optimizer").minimize(self.getLoss())

            one_hot_indexes = np.zeros((self.batch_size, self.number_of_actions))
            one_hot_indexes[np.arange(self.batch_size), np.array(indexes)] = 1

            trainStep, loss = self.sess.run([self.train_step, self.loss], \
                feed_dict = {self.targetValues: y_j, self.inputData: phi0s, self.action: one_hot_indexes})

            return loss
        else:
            return None

    def save(self):
        # TODO: saving also target, which is unnecessary.
        if self.model_params_path is not None:
            self.saver.save(self.sess, self.model_params_path + ".ckpt")
        else:
            print("Model not saved since target directory is not given.")
