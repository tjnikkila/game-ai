import gym
import universe
import sys
import tensorflow as tf
import sonnet as snt
import numpy as np

from dqn.actionManager import ActionManager
#from dqn.qHandler.actionsInputHandler import ActionsInputHandler as ActionHandler
from dqn.qHandler.actionsOutputHandler import ActionsOutputHandler as ActionHandler
from dqn.preProcessor import PreProcessor
from dqn.replayMemory.ReplayMemory import ReplayMemory
from dqn.indexToActions import IndexToActions
from util.observationFunctions import   getFocusedDownScaledBWImage, \
                                        getFocusedDownScaledBrownImage, \
                                        getFocusedDownScaledImage, \
                                        getFocusedDownScaledFftImage, \
                                        getFocusedDownScaled_140_200_Image
from util.benchmark import MethodBenchmark

# REVIEW:
# patterns ended up to be bad since they were biased to left

# How much initial pattern balance effect in a long run?

class Dqn:
    def run(self, iteration_count, race_count, model_params_path, log_mode, exploration_probability = 0.3,\
            learning_rate=1e-9):
        env = gym.make('flashgames.CoasterRacer-v0')
        env.configure(remotes=1)

        batch_size = 20
        renders_per_action = 10

        pre_processor = PreProcessor(4, getFocusedDownScaled_140_200_Image)
        index_to_actions = IndexToActions()
        action_manager = ActionManager(renders_per_action, env)
        replay_memory = ReplayMemory(transitions_max = 1500, zero_rule = False, batch_size = batch_size)

        current_iteration = 0
        since_optimized = 0
        current_phi = None
        previous_phi = None
        pattern_index = None

        with tf.Session() as sess:
            methodBenchmark = MethodBenchmark(race_count, "removeMeRaceSummary.txt", statistics_file_mode = log_mode)

            q_handler = ActionHandler(sess, inputShape = (None, 140, 200, 12), batchSize = batch_size, replayMemory = replay_memory,\
                reset_interval = 1500, gamma = 0.9, learning_rate=learning_rate, keep_prob = tf.constant(0.8),\
                explorationProbability = exploration_probability, model_params_path = model_params_path, is_training = True)
            q_handler.setUp()

            while True:
                if current_iteration > iteration_count:
                    break

                if current_phi is None or previous_phi is None:
                    actions = index_to_actions.forwards()
                else:
                    pattern_index = q_handler.nextIndex(current_phi)
                    print("new pattern id " + str(pattern_index))
                    actions = index_to_actions.toActions(pattern_index)

                action_manager.addPattern(actions)
                while True:
                    observation, reward, patterns_left, renders_left, race_done = action_manager.render()
                    if methodBenchmark.renderPlain(race_done, reward, observation):
                        return

                    if race_done:
                        q_handler.save()
                    current_iteration += 1
                    if not renders_left:
                        methodBenchmark.add_reward(reward)
                        break

                # TODO: rename since_optimized
                since_optimized += 1
                previous_phi = current_phi
                current_phi = pre_processor.process(observation)
                if current_phi is not None and previous_phi is not None and pattern_index is not None:
                    replay_memory.add_transition(previous_phi, pattern_index, reward, current_phi)
                if since_optimized > 1000 and since_optimized % 3 == 0:
                    loss = q_handler.optimize()
                    if loss is not None:
                        loss = np.sum(loss)
                    methodBenchmark.add_loss(loss)
                    print("Optimization loss: " + str(loss))
                    # since_optimized = 0

                if current_iteration % 100 == 0:
                    print("Current interation " + str(current_iteration))
