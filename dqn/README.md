### Deep Q Network(DQN)

BASIC FUNCTIONALITIES ARE IMPLEMENTED, BUT NOT OPTIMISED YET.

Theory of how to implement DQN is taken from page 15 of [whitepaper](https://arxiv.org/pdf/1701.07274.pdf) that studied state of the art deep reinforced learning.

Rough design of the solution is shown in `Figure 1`

_Figure 1_
![alt text](documents/dqn_design.png "Figure 1")

Running on universe environment showed that learning is bad and slow. I made some [hack](bootstrapDqn.py) to make some pre-learning. Benefits of pre learning by hacking environment seems to be limited compared to time it takes.

Actual model training and results by running atari game on universe are coming soon.

To test:
* how well default model learns to play the game.
* how fast DQN learns
* how stabile DQN is. If trained multiple times, then how learning curve behaves from training to another.
* is best patterns better than left-right balanced patterns
* what if conv2model is made deeper.
* what if kernel shape is changed
* what if phi(ϕ) is 5 or 3 images instead of 4
* what if phi has images twice as long apart from each other. Or half as far.
* remove softmax at the end of model
* how well loss compare to time and reward in race.
* can image be preprocessed in a way to help model learn. For example take convert image to show difference between pixels. Maybe road is then more obvious.
* try to make training using batches while actual prediction is using single transitions
* Could GradientDescentOptimizer be replaced by AdamOptimizer?
* what if transition phis do not overlap
* decreasing exploration/exploitation rate
* How adding baseline changes DQN
* Q parameter values are copied to Q_target on every 40 optimization. What if that happens more often or rare? If very often then model should become more unstable.
* What makes DQN good or special? Why it learns or not learn?
* drop out only at one layer
* smaller drop out
* L1 and L2
* add fully connected layer
* add/remove one conv2
* black&white vs grb
* pre train model
* action manager delays phis.
* increase replaymemory size
* give cumulative reward as long as reward keeps incresing. So good earlier decisions get good rewards
* normalize input images
* try elu
* add conv2 layers
* fully connected layers that gets smaller and smaller
* Increase point if going fast and see how it changes learning and behaviour
