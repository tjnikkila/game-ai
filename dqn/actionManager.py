from queue import Queue

class ActionManager:
    def __init__(self, renders_per_action, env):
        self.renders_per_action = renders_per_action
        self.actions = Queue()
        self.env = env
        self.observations = env.reset()
        self.currentActionSet = Queue()
        self.rendersLeft = 0
        self.currentAction = None
        self.cumulativeReward = 0
        self.previous_observation = None

    def observation_defined(self, observation):
        return observation is not None and observation != [None] and len(observation) > 0

    def render(self):
        if not self.rendersLeft:
            if not self.currentActionSet.qsize():
                if not self.actions.qsize():
                    raise Exception("Run out of actions")
                else:
                    for action in self.actions.get():
                        self.currentActionSet.put(action)
                    self.cumulativeReward = 0

            self.currentAction = self.currentActionSet.get()
            self.rendersLeft = self.renders_per_action

        is_done = False
        self.observations = [None]
        while self.observation_defined(self.observations) == False:
            action_n = [self.currentAction for ob in self.observations]
            self.observations, rewards, done_n, info = self.env.step(action_n)

            # NB: Something weird going on with observations
            if self.observation_defined(self.observations):
                self.previous_observation = self.observations
            if done_n[0]:
                is_done = True
                break
        self.cumulativeReward += rewards[0]
        self.rendersLeft -= 1
        if not self.observation_defined(self.observations):
            print("Observation was not defined. Previous observation taken")
            self.observations =  self.previous_observation

        rendersLeft = self.currentActionSet.qsize() * self.renders_per_action + self.rendersLeft
        return self.observations, self.cumulativeReward, self.actions.qsize(), rendersLeft, is_done

    def addPattern(self, actions):
        self.actions.put(actions)
