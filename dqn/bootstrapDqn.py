# Why breaks if import universe is removed?
#import universe
import sonnet as snt
import tensorflow as tf
from queue import Queue
import numpy as np

from dqn.qHandler.actionsOutputHandler import ActionsOutputHandler
from dqn.preProcessor import PreProcessor
from dqn.replayMemory.ReplayMemory import ReplayMemory
from util.observationFunctions import getFocusedDownScaledBWImage

from util.devObservations import DevObservations

def run_bootstrap(model_params_path):
    pre_processor = PreProcessor(4, getFocusedDownScaledBWImage)
    replay_memory = ReplayMemory()
    current_iteration = 0
    current_phi = None
    previous_phi = None
    pattern_index = None
    base_gamma = 0.3
    base_learning_rate = 1e-8

    with tf.Session() as sess:
        q_handler = ActionsOutputHandler(sess, (1, 70, 100, 4), 1, replay_memory, stepsToResetQTarget = 10, gamma = 0.3, \
            explorationProbability = 0.3, model_params_path = model_params_path, learning_rate=1e-8)
        q_handler.setUp()
        dataIO = DevObservations()
        current_iteration = 0
        losses = np.ones(1000)*5
        q_handler.gamma = base_gamma
        q_handler.learning_rate = base_learning_rate

        # lataa kuvat:
        for i in range(4):
            obs, rew, _ = dataIO.donwload_next_pattern()
            vis = [{'vision': obs}]
            phi0 = pre_processor.process(vis)
            current_iteration += 1

        cumulative_loss = 0

        if True:
            #alusta tiedosto
            # määritä gamma [0.1:1.0]
            # käy läpi 100000.
            # tallenna loss per 1000 tiedostoon omille riveilleen. Yhteensä 100 riviä
            with open("bootstappingGamma.txt", "w", 1) as file_handle:
                for gamma in np.arange(0.86, 1.0, 0.02):
                    current_iteration = 0
                    q_handler.gamma = gamma
                    for _ in range(100):
                        for i in range(50):
                            obs, rew, ind = dataIO.donwload_next_pattern()
                            phi1 = pre_processor.process([{'vision': obs}])
                            replay_memory.add_transition(phi0, ind, rew, phi1)
                            phi0 = phi1

                        for _ in range(50):
                            loss = q_handler.optimize()
                            losses[current_iteration % 1000] = loss
                            cumulative_loss += loss

                            current_iteration += 1
                            if current_iteration % 100 == 0:
                                print("Current iteration " + str(current_iteration))
                                print("Gamma value " + str(gamma) + "\n")
                            if current_iteration % 1000 == 0:
                                file_handle.write("Gamma value " + str(gamma) + "\n")
                                file_handle.write("Current iteration " + str(current_iteration) + "\n")
                                file_handle.write("Current loss " + str(losses.mean()) + "\n")
                                file_handle.write("Average loss " + str(cumulative_loss/current_iteration) + "\n")
                                file_handle.write("\n")

        if True:
            q_handler.gamma = base_gamma
            q_handler.learning_rate = base_learning_rate
            cumulative_loss = 0
            losses = np.ones(1000)*5
            with open("bootstappingLearningRate.txt", "w", 1) as file_handle:
                for lr in np.logspace(-10, -1.0, num=5):
                    current_iteration = 0
                    q_handler.learning_rate = lr
                    for _ in range(120):
                        for i in range(50):
                            obs, rew, ind = dataIO.donwload_next_pattern()
                            phi1 = pre_processor.process([{'vision': obs}])
                            replay_memory.add_transition(phi0, ind, rew, phi1)
                            phi0 = phi1

                        for _ in range(50):
                            loss = q_handler.optimize()
                            losses[current_iteration % 1000] = loss
                            cumulative_loss += loss

                            current_iteration += 1
                            if current_iteration % 100 == 0:
                                print("Current iteration " + str(current_iteration))
                                print("learning_rate value " + str(lr) + "\n")
                            if current_iteration % 1000 == 0:
                                file_handle.write("Learning rate value " + str(lr) + "\n")
                                file_handle.write("Current iteration " + str(current_iteration) + "\n")
                                file_handle.write("Current loss " + str(losses.mean()) + "\n")
                                file_handle.write("Average loss " + str(cumulative_loss/current_iteration) + "\n")
                                file_handle.write("\n")

        q_handler.save()
