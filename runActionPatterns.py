import argparse
from findActionPattern.actionFinder import ActionFinder

parser = argparse.ArgumentParser()
parser.add_argument("--patternsTo", help="Folder path to save found pattern statistics. Default is './patterns'",
                    type=str)
parser.add_argument("--rewardSystem", help="Choose reward system. Options are 'basic' and 'deferred'. Default is 'basic'",
                    type=str)
args = parser.parse_args()
rewardSystem = args.rewardSystem
patternsTo = args.patternsTo

ActionFinder().run(patternsTo, rewardSystem)
