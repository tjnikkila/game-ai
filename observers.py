# Observers can be given to different atari game logic to perform tasks that are
# independed from the game logic. For example collecting expert training data
import numpy as np
import json
import sys
from pprint import pprint
from util.observationFunctions import getFocusedDownScaledBWImage

class ObserverUtil(object):
    def __init__(self, toFolder):
        self.fileCount = 0
        self.toFolder = toFolder
        self.metaPerObs = []

    # palauttaa fileCount
    def saveFile(self, observation, customMetaDict):
        self.fileCount += 1
        fileName = self.toFolder + "/train" + str(self.fileCount) + ".pyd"
        with open(fileName,"wb") as obsFile:
            np.save(obsFile, observation)

        commonMeta = {'name': fileName}
        self.metaPerObs.append({**commonMeta, **customMetaDict})

    def saveMetaFile(self):
        with open(self.toFolder + "/nameReward.json","w") as outfile:
            json.dump(self.metaPerObs, outfile, sort_keys = True, indent = 4, ensure_ascii = False)

class EmptyObserver:
    def observing(self, obs, reward, iterationCount, actionIndex=None, customMap=None):
        return True

    def requiredIterations(self):
        return 0

# Testing python inheritance
class Every50Observer(ObserverUtil):
    def __init__(self, setSize, toFolder):
        super(Every50Observer, self).__init__(toFolder)
        self.setSize = setSize
        self.picPerIteration = 50

    def requiredIterations(self):
        return self.picPerIteration * self.setSize

    def observing(self, obs, reward, iterationCount, actionIndex=None, customMap=None):
        if iterationCount % self.picPerIteration == 0:
            self.saveFile(obs, {'reward': reward})

        if self.fileCount >= self.setSize:
            self.saveMetaFile()
            return False
        else:
            return True

class WithMetaObserver(ObserverUtil):
    def __init__(self, setSize, toFolder):
        super(WithMetaObserver, self).__init__(toFolder)
        self.setSize = setSize

    def observing(self, observation, reward, iterationCount, actionIndex, customMap):
        observation = getFocusedDownScaledBWImage(observation)
        self.saveFile(observation,{**customMap, 'reward': int(reward), 'actionIndex': int(actionIndex), 'iterationCount': int(iterationCount)})

        if self.fileCount >= self.setSize:
            self.saveMetaFile()
            return False
        else:
            return True
