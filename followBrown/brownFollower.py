import gym
import universe
import random
import numpy as np
from util.benchmark import MethodBenchmark

class BrownFollower:
    # observer takes observation and reward and iteration count as a paramter
    def run(self, observer, iterationCount, raceCount):
        startingUpTime = 2500
        assert(iterationCount > observer.requiredIterations() + startingUpTime)
        env = gym.make('flashgames.CoasterRacer-v0')
        env.configure(remotes=1)
        observation_n = env.reset()
        # num of game iterations
        n = 0

        #define our turns or keyboard actions
        left = [('KeyEvent', 'ArrowUp', True) ,('KeyEvent', 'ArrowLeft', True), ('KeyEvent', 'ArrowRight', False)]
        right = [('KeyEvent', 'ArrowUp', True) ,('KeyEvent', 'ArrowLeft', False), ('KeyEvent', 'ArrowRight', True)]
        Forward = [('KeyEvent', 'ArrowUp', True) ,('KeyEvent', 'ArrowLeft', False), ('KeyEvent', 'ArrowRight', False)]

        def closeToBrown(color):
            brown = np.array([155, 148, 80])
            return (np.abs(color-brown)<15).all() # 20 -> 2:20

        methodBenchmark = MethodBenchmark(raceCount)

        event = Forward
        for i in range(iterationCount):
            #increment a counter for number of iterations
            n+=1

            #if at least one iteration is made, check if turn is needed
            if(n > 1):
                #if at least one iteration, check if a turn
                if(observation_n[0] != None):
                    leftHalf = observation_n[0]['vision'][500,0:345,:]
                    rightHalf  = observation_n[0]['vision'][500,345:1024,:]

                    rightscore = np.sum(np.apply_along_axis(closeToBrown,1,rightHalf))
                    leftscore = np.sum(np.apply_along_axis(closeToBrown,1,leftHalf))
                    if(rightscore > leftscore + 3):
                        event = right
                    elif(rightscore < leftscore - 5): # -3:lla 2.20 -> 1.52
                        event = left
                    else:
                        if(n%3 == 0):
                            event = Forward

                    #perform an action
                    action_n = [event for ob in observation_n]

            else:
                #if no turn is needed, go straight
                action_n = [Forward for ob in observation_n]

            if n % 500 == 0:
                print("Iteration " + str(n) + "/" + str(iterationCount) )

            if(observation_n[0] != None and observer.observing(observation_n[0]['vision'], reward_n, n) is False):
                break

            #save new variables for each iteration
            observation_n, reward_n, done_n, info = env.step(action_n)
            benchmarkingDone = methodBenchmark.render(done_n, reward_n, observation_n)
            if benchmarkingDone:
                break

        methodBenchmark.printResults()
