from random import randint
import gym
import universe
from pprint import pprint
from util.benchmark import MethodBenchmark
from findActionPattern.bestPatterns import forward, patterns

class SelectRandomPattern:
    def run(self, iterationCnt, raceCount):
        env = gym.make('flashgames.CoasterRacer-v0')
        env.configure(remotes=1)
        observation = env.reset()
        currentPatternIndex = 0
        currentActionIndex = 0
        takeAction = forward
        methodBenchmark = MethodBenchmark(raceCount)
        for i in range(iterationCnt):
            if(i > 1 and observation[0] is not None):
                if(i % 5 == 0):
                    #take next action
                    if(i % (5*5) == 0):
                        # select new pattern
                        currentPatternIndex = randint(0, 4)
                        currentActionIndex = 0

                    takeAction = patterns[currentPatternIndex][currentActionIndex]
                    currentActionIndex = (currentActionIndex + 1) % 5 # there are 5 actions to take.
            action_n = [takeAction for ob in observation]
            observation, rewards, done_n, info = env.step(action_n)
            benchmarkingDone = methodBenchmark.render(done_n, rewards, observation)
            if benchmarkingDone:
                break

        methodBenchmark.printResults()
