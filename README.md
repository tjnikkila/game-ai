## About

Teaching computer to play Atari games. OpenAi's [universe](https://github.com/openai/universe)
project is the framework to run games. Building solution to run against all games is too big challenge at the begin. So at first stage solutions are run agains `flashgames.CoasterRacer-v0`, which is a car driving game.


For the rest of the document, word `iteration` refers to one observation and reward -pair. There are few dozens iteration in one second.

## Set Up

Code is to be run by python 3.6 (also pip3)

Install universe. Instruction is [here](https://github.com/openai/universe).

Clone game logic to your local machine:
```sh
git clone https://gitlab.com/tjnikkila/game-ai.git
```
Go to cloned directory `cd game-ai`

Logics are stored in subdirectories. In a case particular logic requires pip installations
you will find requirements.txt file in that direcotry.
To install dependencies, run
```sh
pip install -r requirements.txt
```

## Available commands

Each solution has a run file in root directory. List of run commands of current solutions. Solutions are explained later:
```sh
python runFollowBrown.py
python runRandomPattern.py
python runSmoothDistDeepQ.py
```

In some cases you can fine tune particular logic. Fine tuning happens giving parameters.
To find out what can be parameterized run command
```sh
python <select-python-file>.py -h
```

## Solving the problem

[How to measure model performance](documentation/performance.md).

Environment is 2,7 GHz Intel Core i7 and 16 GB 2133 MHz LPDDR3. Training and testing are run on CPU.

First solution is to test something simple to learn how system is working functionally and non-functionally. First solution is to decide should car turn left or right based on which side of the observed game view, left or right, has more brown pixels in a measured row. Not that game view is observation where white are is removed from right and bottom.
```sh
python runFollowBrown.py --raceCount 10
```
Reason for this is that road has roughly all brown pixels in the game. Results are pretty good. Solution had reward mean 9300 and std 2400.
Solution scaled to observe amount of brown pixels in few rows, after which computation became so heavy computer could not play real time. Few shortcuts were made to make further development easier: First downscaling images from original shape of (768, 1024, 3) to (70, 100, 3). Road can still be observed in picture of size (70, 100, 3). `Image 1` shows example of original observation and `image 2` shows downscaled image.

_Image 1_
![alt text](documentation/exampleObservation.png "Observation")

_Image 2_
![alt text](documentation/exampleDownscaled.png "Downscaled observation")


Second method to boost performance is analysing and taking action much rearer than every iteration. Decision was made to analyse every 5th observation. Third, it is obvious to human observer that good actions include going forward. So there could be pattern of actions that work well together. [Best 5 action patterns were searched and found](findActionPattern/). Now each action takes 5 iterations and each pattern consist of 5 actions. So new patterns needs to be analysed every 25 iterations.

[Mean reward of picking random patterns is 6700 and std is 1400](randomPattern/).
```sh
python runRandomPattern.py --raceCount 10
```

Motivation for next solution is to get hands on experience in deep reinforcement learning as fast as possible. To learn deep reinforcement learning engineering including using tools and to gain some instinct in this field. Meanwhile studying theories of what have been done and what is working. [Solution](smoothDist/) uses 2 convolutional layers to define current state from current observation. Simple one step Q-learning is trained by states and rewards and taken actions. Q-learning is used to define value of possible patterns in particular state. Policy to take pattern favours exploring at the begin and being greedy after a while.
```sh
python runSmoothDistDeepQ.py --raceCount 10
```
This solution gained mean reward of 5900 and std of 1400 during the first 10 race and mean of 4500 and std of 800 during races between 10 and 20. Dnn loss was roughly the same in all races.

Vision observation contains wide useless surroundings. Performance improvement is searched by removing extra surroundings. `Image 3` illustrates visual observation after removing extra surroundings and downscaling.

_Image 3_
![alt text](documentation/focusedDownscaled.jpg "Cleaned downscaled observation")

Re-running
```sh
python runSmoothDistDeepQ.py --raceCount 10 --focusObservation True
```
to test if observation cleaning improved performance. Run achieved mean reward of 5500 and std of 1500. No improvement compared to previous run.

### Compare Off-Policy Deep Q-Network (DQN) and On-Policy Actor-Critic

Implement on-policy and off-policy methods to get deeper understanding on what is policy. DQN was the first stable bootstrapping off-policy function approximator. Also it is rather basic version of such. Actor-critic is well known and stable version of on-policy methods.

#### DQN

Basic version is [implemented](dqn/) and parameter tuning is on going. Results are coming soon. Coming versions will have more optimized functionalities like adding baseline.

DQN implementation is still going to evolve, but current version can be run by:
```sh
python runDqn.py
```

#### Next steps:

Study theory and gain ideas where to invest next. Then make a plan and implement it. Main source of ideas: https://www.youtube.com/playlist?list=PLkFD6_40KJIwTmSbCv9OVJB3YaO4sFwkX
https://arxiv.org/pdf/1701.07274.pdf
