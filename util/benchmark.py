import numpy as np
from functools import partial
import argparse

class EpochInterrupted(Exception):
    pass

class MethodBenchmarkParams:
    @staticmethod
    def isRaceCountType(value):
        if value is not None:
            ivalue = int(value)
            if ivalue <= 0:
                 raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
            return ivalue
        else:
            return None

class CustomWriter:
    def __init__(self, method):
        self.method = method
    def __call__(self, line):
        self.method(line + "\n")

class MethodBenchmark:
    def __init__(self, raceCount, result_file = None, statistics_file_mode = "w"):
        self.spendTime = 0
        self.currentEpisodeId = None
        self.history = []
        self.reward = 0
        self.loss = []
        self.raceCount = raceCount
        self.result_file = result_file
        if self.result_file is not None:
            with open(self.result_file, statistics_file_mode) as file_handle:
                file_handle.write("")
    def add_reward(self, reward):
        self.reward += reward
    def add_loss(self, loss):
        if loss is not None:
            self.loss.append(loss)

    def renderPlain(self, race_done, reward, observation):
        return self.render([race_done], [0], [observation])

    def render(self, done, reward, observation):
        if observation is not None and observation[0] is not None:
            self.spendTime += 1
            self.reward += reward[0]

        if done[0] == True:
            self.history.append({"time": self.spendTime, "reward": self.reward})
            self.spendTime = 0
            self.reward = 0
            if self.result_file is not None:
                self.writeResults()
            else:
                self.printResults()
            if(self.raceCount is not None and self.raceCount == len(self.history)):
                return True

        return False

    def writeResults(self):
        stats = self.get_stats()
        with open(self.result_file, "a") as file_handle:
            for stat in stats:
                print(stat)
                file_handle.write(str(stat) + "\n")

    def printResults(self, method = print):
        stats = self.get_stats()
        for stat in stats:
            print(stat)

    def get_stats(self):
        stats = []
        stats.append("PERFORMANCE SUMMARY")
        stats.append("Number of games: " + str(len(self.history)))
        if len(self.history) > 0:
            stats.append("Time and reward per competition: ")
            stats.append(str(self.history))

            times = [a for a in map(lambda b: b['time'], self.history)]
            stats.append("Mean time (in observations) %d and standard deviation %d." % (np.mean(times), np.std(times)))
            rewards = [a for a in map(lambda b: b['reward'], self.history)]
            stats.append("Mean reward %d and standard deviation %d." % (np.mean(rewards), np.std(rewards)))
            if self.loss:
                stats.append("Mean loss %d and standard deviation %d. Times optimized %d" % (np.mean(self.loss), np.std(self.loss), len(self.loss)))

            # Reward not increasing
            if max(times) > 60000 and (np.mean(rewards) * 1.2 ) > max(rewards):
                max_info = "Max time was " + str(max(times)) + ". reward " + str(max(rewards))
                print(max_info)
                raise Exception("Took too long to run one race. Took " + str(max(times)))

            # universe start collapsing after a while. Needs to be restarted.
            elif rewards[-1] < 1000:
                raise EpochInterrupted("Reward drop near zero")

        return stats
