import numpy as np

class EnvEmulator:
    def __init__(self, observationMetaData):
        self.observationMetaData = observationMetaData
        self.fileNr = 1

    def configure(self, remotes):
        return {}

    def reset(self):
        return [None]

    def getCurrentMeta(self):
        nextObservation = self.fileNr % len(self.observationMetaData)
        return self.observationMetaData[nextObservation]

    def step(self, action_n):
        # reward = rewards[0]
        # observation[0]['vision']
        # observations, rewards, done_n, info = env.step(action_n)

        nextObservation = self.fileNr % len(self.observationMetaData)
        self.fileNr += 1
        meta = self.observationMetaData[nextObservation]
        obs = np.load(meta['name'])
        envObs = [{'vision': obs}]
        rewards = [meta['reward']]
        return envObs, rewards, False, None

    def reset(self):
        return [None]
