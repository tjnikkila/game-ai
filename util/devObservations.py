import numpy as np
import json
import ast
from scipy.misc import imresize

class DevObservations:
    def __init__(self):
        self.current = 0
        self.start_from = 1
        with open('./expertTrainData/nameReward.json') as nameRewardFile:
            fileStr = nameRewardFile.read()
            nameReward = ast.literal_eval(fileStr)
            self.images_cnt = len(nameReward)

    def donwload_next_pattern(self):
        pattern_size = 5
        contains_one_whole_pattern = 9

        meta_row = self.__get_meta_data()

        if self.current + contains_one_whole_pattern >= self.images_cnt:
            self.current = 0

        current_pattern = None
        current_pattern_count = 0
        possible_rows = meta_row[self.current:]
        for row in possible_rows:
            self.current += 1
            if current_pattern is None:
                current_pattern = row["patternIndex"]
            else:
                pattern_idx = row["patternIndex"]
                if pattern_idx == current_pattern:
                    current_pattern_count += 1
                else:
                    current_pattern_count = 0

                if current_pattern_count == pattern_size:
                    break

        selected_patterns = meta_row[self.current - pattern_size: self.current]
        obs = self.__load_pictures([selected_patterns[-1]["name"]], down_scale = False)[0]
        ind = selected_patterns[-1]["patternIndex"]
        reward = sum([r["reward"] for r in selected_patterns])

        return obs, reward, ind

    def __load_pictures(self, file_names, down_scale):
        pics = []
        for file_name in file_names:
            with open(file_name, 'rb') as outfile:
                pic = np.load(outfile)
                if down_scale:
                    pic = imresize(pic, (70, 100, 3))
                pics.append(pic)
        return pics

    def __get_meta_data(self):
        with open('./expertTrainData/nameReward.json') as nameRewardFile:
            fileStr = nameRewardFile.read()
            return ast.literal_eval(fileStr)

    def images(self, fromIm, toIm, *, down_scale = False):
        nameReward = self.__get_meta_data()
        if fromIm is None:
            nameReward = nameReward[fromIm:toIm]
        # [a for a in map(lambda a : a+9, u)]
        fileNames = [a for a in map(lambda a: a["name"], nameReward)]
        rewards = [a for a in map(lambda a: a["reward"], nameReward)]
        indices = [a for a in map(lambda a: a["patternIndex"], nameReward)]


        pics = self.__load_pictures(fileNames, down_scale)
        return np.array(pics).astype(np.float32), rewards, indices


    def downScaledImages(self, fromIm, toIm):
        return self.images(fromIm, toIm, down_scale = True)

    # from and to must be between 0 and 20000
    def getObservationMetaData(self, fromObs, toObs):
        with open('./expertTrainData/nameReward.json') as nameRewardFile:
            fileStr = nameRewardFile.read()
            return ast.literal_eval(fileStr)[fromObs:toObs]
