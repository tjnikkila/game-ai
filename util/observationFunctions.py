from scipy.misc import imresize
from skimage import color
import numpy as np
from pprint import pprint

grey = "grey"
brown ="brown"
fft = "fft"

def getRgbObs(observation):
    return observation[0]['vision']

def focusCoasterRacer(observation):
    return observation[85:560,35:640,:]

def asDownScaledImage(observation, convertion, target_size = (70, 100, 3)):
    resized = (imresize(observation, target_size))
    if convertion == grey:
        return color.rgb2gray(resized)
    if convertion == brown:
        return rgb2Brown(resized)
    if convertion == fft:
        reduc_dim = color.rgb2gray(resized)
        return  abs(np.fft.fft2(reduc_dim))
    else:
        return resized

def getFocusedDownScaled_140_200_Image(observation):
    return getFocusedDownScaledImage(observation, color, (140, 200, 3))

def getFocusedDownScaledImage(observation, color=None, target_size = (70, 100, 3)):
    rgbObs = getRgbObs(observation)
    focused = focusCoasterRacer(rgbObs)
    down =  asDownScaledImage(focused, color, target_size)
    return down / 255

def getFocusedDownScaledFftImage(observation, target_size = (70, 100, 3)):
    return getFocusedDownScaledImage(observation, fft)

def getFocusedDownScaledBrownImage(observation, target_size = (70, 100, 3)):
    return getFocusedDownScaledImage(observation, brown)

def getFocusedDownScaledBWImage(observation, target_size = (70, 100, 3)):
    return getFocusedDownScaledImage(observation, grey, target_size = target_size)

def getDownScaledImage(observation):
    rgbObs = getRgbObs(observation)
    return asDownScaledImage(rgbObs, False)

def rgb2Brown(observation):
    return np.apply_along_axis(closeToBrown,2,observation)

def closeToBrown(observation):
    brown = np.array([155, 148, 80])
    return (np.abs(observation-brown)<15).all()
