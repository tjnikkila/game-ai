import argparse
from followBrown.brownFollower import BrownFollower
from smoothDist.smoothDistDeepQ import SmoothDistDeepQ
from observers import Every50Observer, WithMetaObserver

parser = argparse.ArgumentParser(description = """
Returns Void.
Side effects:
saves N number of train images to given path. Overrides if exists.
Saves JSON file that contains name of image files and possibly some extra information:
[
    {
        "name": "./expertTrainData/train1.pyd",
        "reward": [
            0.0
        ]
    },
    {
        "name": "./expertTrainData/train2.pyd",
        "reward": [
            0.0
        ],
        ...
    }]
""")

parser.add_argument("--toFolder", help="Folder path to save training files",
                    type=str, required=False, default='expertTrainData')
parser.add_argument("--setSize", help="How many training images to generate",
                    type=int, required=False, default=1000)
parser.add_argument("--runName", help="Which train data to generate? 'every50' "\
  "for collecting every 50 observation. 'withMetaSmooth' for save with custom metadata ",
                    type=str, required=False, default='withMetaSmooth')
args = parser.parse_args()
toFolder = args.toFolder
setSize = args.setSize

if args.runName == 'every50':
    obs = Every50Observer(setSize, toFolder)
    BrownFollower().run(obs, 200000, None)
elif args.runName == 'withMetaSmooth':
    obs = WithMetaObserver(setSize, toFolder)
    SmoothDistDeepQ().run(200000, 10, False, obs)
else:
    raise Exception("Given runName \"" + args.runName + "\" is illegal." )
