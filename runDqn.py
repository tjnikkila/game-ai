import argparse
#from util.devObservations import DevObservations
from util.envEmulator import EnvEmulator
from util.benchmark import EpochInterrupted
from dqn.dqn import Dqn
from dqn.bootstrapDqn import run_bootstrap
from pprint import pprint
import time
import os
import shutil

start = time.time()

parser = argparse.ArgumentParser()
parser.add_argument("--bootstap", help="If True, then trains Q model against " + \
    "saved test data in expertTrainData -directory", type=bool, default=False)

parser.add_argument("--log_mode", help="If 'w' then clear old perfomance log file." + \
    " If 'a' then appends.", type=str, default="w")
parser.add_argument("--exploration_probability", help="Probability for exploration instead of exploitation.", \
    type=float, default=0.3)
parser.add_argument("--learning_rate", help="learning_rate", \
    type=float, default=1e-9)
args = parser.parse_args()
bootstap = args.bootstap
log_mode = args.log_mode
learning_rate = args.learning_rate
exploration_probability = args.exploration_probability

rootFold = "./dqn/modelParams"
folderName = rootFold + '/dqn'
if bootstap:
    run_bootstrap(None)
else:
    # TODO: remove
    #if os.path.exists(rootFold):
    #    shutil.rmtree(rootFold)
    #os.makedirs(rootFold)
    dqn = Dqn()
    dqn.run(10000000, 10, folderName, log_mode = log_mode, exploration_probability = exploration_probability,\
        learning_rate=learning_rate)
    """
    epoch_count = 4
    first_epoch = True
    for i in range(epoch_count):
        dqn = Dqn()
        try:
            dqn.run(10000000, 1, folderName, log_mode = log_mode)
        except EpochInterrupted as e:
            print("Epoch was interrupted")
        finally:
            first_epoch = False
    """



end = time.time()
print("Total time " + str(end - start))
