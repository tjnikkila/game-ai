import numpy as np

class EnvTest:
    def __init__(self):
        self.observations = np.arange(0,80).reshape(5,4,4)
        self.fileNr = 0
        self.observationIndex = 0

    def getCurrentImage(self):
        return self.observations[self.observationIndex]

    def step(self, action_n):
        # reward = rewards[0]
        # observation[0]['vision']
        # observations, rewards, done_n, info = env.step(action_n)

        self.observationIndex = self.fileNr % self.observations.shape[0]
        self.fileNr += 1
        obs = self.observations[self.observationIndex]
        envObs = [{'vision': obs}]
        rewards = [self.observationIndex]
        return envObs, rewards, [False], None

    def reset(self):
        return [None]
