from util.devObservations import DevObservations
from util.envEmulator import EnvEmulator
import numpy as np
from util.observationFunctions import getFocusedDownScaledImage

class ReplayMemory:
    def __init__(self):
        observations1 = np.load('tests/resources/train1.pyd')
        reward1 = 0
        observations2 = np.load('tests/resources/train2.pyd')
        reward2 = 1
        observations3 = np.load('tests/resources/train3.pyd')
        reward3 = 0
        observations4 = np.load('tests/resources/train4.pyd')
        reward4 = 1
        observations5 = np.load('tests/resources/train5.pyd')
        reward5 = 0
        observations6 = np.load('tests/resources/train6.pyd')
        reward6 = 3

        patternIndex4 = 2
        preprocessedT = [np.dstack((observations1, observations2, observations3, observations4))]
        preprocessedTPlus1 = [np.dstack((observations2, observations3, observations4, observations5))]
        patternIndex5 = 1
        preprocessedTPlus2 = [np.dstack((observations3, observations4, observations5, observations6))]

        self.phi0s = preprocessedT + preprocessedTPlus1
        self.indexes = [patternIndex4, patternIndex5]
        self.rewards = [0.2,0.1]
        self.phi1s = preprocessedTPlus1 + preprocessedTPlus2

    def get_sample(self):
        return self.phi0s, self.indexes, self.rewards, self.phi1s
