from dqn.actionManager import ActionManager
import unittest
import numpy as np
from tests.src.util.envTest import EnvTest
from findActionPattern.bestPatterns import patterns

class TestActionManager(unittest.TestCase):
    def test_action_manager(self):
        env = EnvTest()
        am = ActionManager(5, env)
        am.addPattern(patterns[0])
        obs, cumulativeReward, patternsLeft, rendersLeft, _  = am.render()
        assert(np.array_equal(obs[0]['vision'], env.getCurrentImage()))
        assert (0 == cumulativeReward)
        assert(patternsLeft == 0)
        assert(rendersLeft == 24)

        am.addPattern(patterns)

        obs, cumulativeReward, patternsLeft, rendersLeft, _  = am.render()
        assert(patternsLeft == 1)
        assert(rendersLeft == 23)
        assert(1 == cumulativeReward)

        obs, cumulativeReward, patternsLeft, rendersLeft, _  = am.render()
        assert (3 == cumulativeReward)

        for loop in range(22):
            obs, cumulativeReward, patternsLeft, rendersLeft, _  = am.render()
        assert(50 == cumulativeReward)

        obs, cumulativeReward, patternsLeft, rendersLeft, _  = am.render()
        assert(np.array_equal(obs[0]['vision'], env.getCurrentImage()))
        assert (0 == cumulativeReward)
        assert(patternsLeft == 0)
        assert(rendersLeft == 24)
