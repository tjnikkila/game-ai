from tests.src.util.ReplayMemory import ReplayMemory
import tensorflow as tf
import numpy as np
import shutil
import os


def dqn_updates_right_parameters(ActionHandler):
    replayMemory = ReplayMemory()

    with tf.Session() as sess:
        batchSize = 2
        inputShape = (None, 70, 100, 4)
        qHandler = ActionHandler(sess, inputShape, batchSize, replayMemory, reset_interval = 3, explorationProbability = 0.1, gamma = 0.05, learning_rate = 1e-5)

        qHandler.setUp()
        qModelTargetVals = sess.run([qHandler.dqnWeights['qModelTarget'].variables["conv2dmod2_w"]])
        qModelVals = sess.run([qHandler.dqnWeights['qModel'].variables["conv2dmod2_w"]])

        assert np.array_equal(qModelVals, qModelTargetVals)
        qHandler.optimize()
        qModelTargetValsAfter = sess.run([qHandler.dqnWeights['qModelTarget'].variables["conv2dmod2_w"]])
        qModelValsAfter = sess.run([qHandler.dqnWeights['qModel'].variables["conv2dmod2_w"]])

        assert np.array_equal(qModelTargetVals, qModelTargetValsAfter)
        assert not np.array_equal(qModelValsAfter, qModelVals)

def dqn_reduces_loss(ActionsHandler):
    def lossBigEnoughForOptimization(loss, learning_rate):
        # If loss is too small then otimizing by learning_step may increase amount of loss.
        return np.all(np.greater(loss, learning_rate * 100))
    replayMemory = ReplayMemory()

    with tf.Session() as sess:
        # TODO: clean batch size implementation in ActionsInputHandler
        batchSize = 2
        inputShape = (None, 70, 100, 4)
        learning_rate = 1e-5
        improvedAccruacy = 0.0000001
        while True:
            qHandler =  ActionsHandler(sess, inputShape, batchSize, replayMemory, reset_interval = 3, explorationProbability = 0.1, gamma = 0.05, learning_rate = learning_rate, keep_prob = 1.0)

            qHandler.setUp()
            lossBefore = qHandler.optimize()

            if lossBigEnoughForOptimization(lossBefore, learning_rate):
                lossAfter = qHandler.optimize()
                # Every optimzation must make loss smaller. However optimization is done in batches. Not all losses in batch have to decrease, but combined loss must.
                assert np.sum(np.subtract(lossBefore, lossAfter)) > improvedAccruacy
                break


def save_and_load_params(ActionsHandler):
    first_model_params = None
    second_model_params = None
    third_model_params = None

    dir_path = cwd = os.getcwd()
    relative_test_result_path =  "/tests/testResults/modelParams"
    model_params_path = ".%s/modelParams" % (relative_test_result_path)
    shutil.rmtree(dir_path + relative_test_result_path)
    replay_memory = ReplayMemory()
    tf.reset_default_graph()
    with tf.Session() as sess:
        batchSize = None
        inputShape = (batchSize, 70, 100, 4)
        learning_rate = 1e-5
        qHandler = ActionsHandler(sess, inputShape, batchSize, replay_memory, reset_interval = 3, \
            explorationProbability = 0.1, gamma = 0.05, learning_rate = learning_rate, keep_prob = 1.0, \
            model_params_path = model_params_path)

        qHandler.setUp()
        first_model_params = sess.run([qHandler.dqnWeights['qModelTarget'].variables["conv2dmod2_w"]])

        qHandler.save()

    tf.reset_default_graph()
    with tf.Session() as sess2:

        q_handler_new_params = ActionsHandler(sess2, inputShape, batchSize, replay_memory, reset_interval = 3, \
            explorationProbability = 0.1, gamma = 0.05, learning_rate = learning_rate, keep_prob = 1.0)

        q_handler_new_params.setUp()

        second_model_params = sess2.run([q_handler_new_params.dqnWeights['qModelTarget'].variables["conv2dmod2_w"]])
        assert not np.array_equal(first_model_params, second_model_params)

    # Note to myself: if session is not explicit graph, then default graph is used and default graph must be reseted between sessions
    #tf.reset_default_graph()
    g = tf.Graph()
    with tf.Session(graph = g) as sess3:
        q_handler_new_params = ActionsHandler(sess3, inputShape, batchSize, replay_memory, reset_interval = 3, \
            explorationProbability = 0.1, gamma = 0.05, learning_rate = learning_rate, keep_prob = 1.0, \
            model_params_path = model_params_path)

        q_handler_new_params.setUp()

        third_model_params = sess3.run([q_handler_new_params.dqnWeights['qModelTarget'].variables["conv2dmod2_w"]])
        assert np.array_equal(first_model_params, third_model_params)
