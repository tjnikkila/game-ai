import unittest
import numpy as np
from dqn.replayMemory.ReplayMemory import ReplayMemory

class TestReplayMemory(unittest.TestCase):
    def test_add(self):
        phi0 = [1,2,3]
        phi1 = [4,5,6]
        phi2 = [7,8,9]
        phi3 = [10,11,12]
        phi4 = [13,14,15]
        r1 = 1
        i1 = 1
        replay = ReplayMemory(transitions_max = 3, batch_size = 2)
        replay.add_transition(phi0, i1, r1, phi1)
        assert(len(replay.phi0s) == 1)
        assert(len(replay.indexes) == 1)
        assert(len(replay.rewards) == 1)
        assert(len(replay.phi1s) == 1)

        replay.add_transition(phi1, i1, r1, phi2)
        replay.add_transition(phi2, i1, r1, phi3)
        replay.add_transition(phi3, i1, r1, phi4)
        assert(len(replay.phi0s) == 3)
        assert(len(replay.indexes) == 3)
        assert(len(replay.rewards) == 3)
        assert(len(replay.phi1s) == 3)


    def test_get_sample(self):
        phi0 = [1,2,3]
        phi1 = [4,5,6]
        phi2 = [7,8,9]
        phi3 = [10,11,12]
        phi4 = [13,14,15]
        r1 = 1
        i1 = 1

        replay = ReplayMemory(transitions_max = 3, batch_size = 2)
        a,b,c,d = replay.get_sample()
        assert(a == None)
        assert(b == None)
        assert(c == None)
        assert(d == None)

        replay.add_transition(phi0, i1, r1, phi1)
        a,b,c,d = replay.get_sample()
        assert(a == None)
        assert(b == None)
        assert(c == None)
        assert(d == None)

        replay.add_transition(phi1, i1, r1, phi2)
        phi0s, indexes, rewards, phi1s = replay.get_sample()
        assert(len(phi0s) == 2)
        assert(len(indexes) == 2)
        assert(len(rewards) == 2)
        assert(len(phi1s) == 2)

        assert(phi0s[0] == phi0 or phi0s[0] == phi1)
        assert(phi0s[1] == phi0 or phi0s[1] == phi1)
