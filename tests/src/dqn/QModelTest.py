from dqn.qHandler.qModelActionOutput import QModelActionOutput
import unittest
import tensorflow as tf
import sonnet as snt
import numpy as np

class TestQModel(unittest.TestCase):

    def test_assignVariables(self):
        batchSize = 1
        inputs = tf.placeholder(tf.float32, shape=(None, 70, 100, 4), name="x1")
        phi = np.random.rand(batchSize, 70, 100, 4).astype(np.float32)

        modelBefore = QModelActionOutput(outputSize = 5)
        modelOutputsBefore  = snt.Module(modelBefore, name='q_model_before')(inputs, is_training=False, keep_prob=tf.constant(0.5))

        modelAfter = QModelActionOutput(outputSize = 5)
        modelOutputsAfter  = snt.Module(modelAfter, name='q_model_after')(inputs, is_training=False, keep_prob=tf.constant(0.5))

        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            conv2dmod2_wBefore = sess.run(modelBefore.variables["conv2dmod2_w"])
            conv2dmod2_wAfter = sess.run(modelAfter.variables["conv2dmod1_w"])
            assert not np.array_equal(conv2dmod2_wBefore, conv2dmod2_wAfter)

            copyTensor = modelBefore.assign(modelAfter)
            conv2dmod2_wCopiedAfter = sess.run(modelBefore.variables["conv2dmod2_w"])
            conv2dmod2_wAfter = sess.run(modelAfter.variables["conv2dmod1_w"])
            assert np.array_equal(conv2dmod2_wBefore, conv2dmod2_wCopiedAfter)
