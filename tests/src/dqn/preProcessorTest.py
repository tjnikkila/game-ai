from dqn.preProcessor import PreProcessor
import unittest
import numpy as np
from tests.src.util.envTest import EnvTest

class TestPreProcessor(unittest.TestCase):

    @staticmethod
    def down_scale_mock(observation):
        return observation

    def test_pre_processor(self):
        processor = PreProcessor(2, self.down_scale_mock)

        image1 = np.array([[1,2],[3,4]])
        image2 = np.array([[11,21],[31,41]])
        image3 = np.array([[12,22],[32,42]])

        first_phi = processor.process(image1)
        assert(first_phi == None)

        second_phi = processor.process(image2)
        assert(np.array_equal(second_phi, np.dstack((image2, image1))))

        third_phi = processor.process(image3)
        assert(np.array_equal(third_phi, np.dstack((image3, image2))))
