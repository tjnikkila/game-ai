from dqn.qHandler.actionsInputHandler import ActionsInputHandler
import unittest
import tensorflow as tf
import sonnet as snt
import numpy as np
from tests.src.util.ReplayMemory import ReplayMemory
from tests.src.dqn.actionsHandlerUtil import dqn_updates_right_parameters, dqn_reduces_loss, save_and_load_params


class TestActionsInputHandler(unittest.TestCase):
    def test_dqnUpdatesRightParameters(self):
        dqn_updates_right_parameters(ActionsInputHandler)

    def test_dqnReducesLoss(self):
        dqn_reduces_loss(ActionsInputHandler)

    def test_saveAndLoadParams(self):
        save_and_load_params(ActionsInputHandler)
