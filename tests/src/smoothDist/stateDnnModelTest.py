import unittest
from smoothDist.stateDnnModel import StateDnnModel
import tensorflow as tf
import numpy as np
from pprint import pprint
import os

import sys

class TestStateDnnModel(unittest.TestCase):
    def getPredictions(self, tfNameSpace):
        dnnMod = StateDnnModel(True)

        tf.reset_default_graph()
        with tf.Session() as sess:
            inputs = tf.placeholder(tf.float32, shape=(2, 70, 100, 3), name="x")
            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):
                train_step = dnnMod.getOptimizer(inputs)
                train_model_outputs = dnnMod.getPredictions(inputs)

            with tf.name_scope("global_initializer"):
                sess.run(tf.global_variables_initializer())

            with open('./tests/resources/testImage1.npy', 'rb') as outfile:
                image1 = np.load(outfile)
            with open('./tests/resources/testImage2.npy', 'rb') as outfile:
                image2 = np.load(outfile)

            observations = np.array([image1, image2])

            for i in range(10):
                tr = sess.run([train_step], feed_dict = {inputs: observations})

            return sess.run(train_model_outputs, feed_dict = {inputs: observations})

    # Verifies that output values are smoothly distributed.
    # Sometimes when this brokes, max value becomes 1.000 and other values
    # be less than 1e-3
    def test_smoothlyDistributed(self):
        predictedState = self.getPredictions("smoothlyDistributed")
        maxValue = predictedState.max()
        minValue = predictedState.min()
        assert(minValue > 0.0001)
        assert(maxValue < 0.9)

    # Sometimes when failing, model gives virtually identical prediction for
    # all inputs
    def test_differentImagesYieldsDifferentPredictions(self):
        predictedState = self.getPredictions("differentImagesYieldsDifferentPredictions")

        diff = np.subtract(predictedState[0], predictedState[1])
        diffCellCount = len(np.where(abs(diff) > 0.001)[0])
        assert(diffCellCount > 0)

if __name__ == '__main__':
    unittest.main()
