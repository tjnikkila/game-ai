import unittest
from smoothDist.qLearningModel import QLearningModelFactory
import numpy as np
from pprint import pprint
class TestQLearningModel(unittest.TestCase):

    def test_updateQtable(self):
        practicallyZero = 0.0001
        qTable = QLearningModelFactory().loadOrInit(None, 10,5)
        assert((qTable.qTable == np.zeros((10, 5), dtype=np.float32)).all())

        # first action is to go from state 0 to state 1 by taking action 2, rewared is 10.
        qTable.updateQValue(0, 10, 2, 1)
        step1State0 = (1-0.3)*0 + 0.3*(10 + 0.5*0) # 3.0
        assert(qTable.getQValue(0, 2) == step1State0)

        # actor takes action 4 from state 1 back to state 0 and gets reward of 15
        qTable.updateQValue(1, 15, 4, 0)
        step2State1 = (1-0.3)*0 + 0.3*(15 + 0.5*step1State0)
        assert(qTable.getQValue(1, 4) - step2State1 < practicallyZero)

        # actor takes action 2 to go from state 0 to state 1
        qTable.updateQValue(0, 10, 2, 1)
        step3State0 = (1-0.3)*step1State0 + 0.3*(15 + 0.5*step2State1)
        assert(qTable.getQValue(0, 2) - step3State0 < practicallyZero)
