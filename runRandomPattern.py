from randomPattern.selectRandomPattern import SelectRandomPattern
import argparse
from util.benchmark import MethodBenchmarkParams

parser = argparse.ArgumentParser()
parser.add_argument("--iterations", help="Number of iterations formula will be run. One observation - reward pair is one iteration. Default is 300000",
                    type=int, default = 300000)
parser.add_argument("--raceCount", help="Max number of races formula will be run. One race is two laps. Default is infinity. Min val is 1",
                    type=MethodBenchmarkParams.isRaceCountType)
args = parser.parse_args()

SelectRandomPattern().run(args.iterations, args.raceCount)
