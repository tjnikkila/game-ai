### Measuring performance

Models are trained based on reward. So rewards is obviously the main method for measuring model performance. Mean and standard deviation (std) of reward is measured over ten races and documented. Also number of iterations is measured but not documented. Number of iteration seems to convert to time. However to be sure, then implementation of time and render should be studied. This study has not been done and will not be done.
