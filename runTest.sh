rm -fr ./tests/src/smoothDist/__pycache__
rm -fr ./tests/src/dqn/__pycache__
rm -fr ./tests/src/__pycache__
rm -fr ./tests/__pycache__
rm -fr ./tests/src/util/__pycache__
rm -fr ./dqn/__pycache__
rm -fr ./dqn/replayMemory/__pycache__
rm -fr __pycache__

rm -fr ./tests/testResults/modelParams/*

python -m unittest tests/src/dqn/*
python -m unittest tests/src/smoothDist/*
