from smoothDist.smoothDistDeepQ import SmoothDistDeepQ
from util.benchmark import MethodBenchmarkParams
from observers import EmptyObserver
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--iterations", help="Max number of iterations formula will be run. One observation - reward pair is one iteration. Default is 300000",
                    type=int, default = 300000)
parser.add_argument("--raceCount", help="Max number of races formula will be run. One race is two laps. Default is infinity. Min val is 1",
                    type=MethodBenchmarkParams.isRaceCountType)
parser.add_argument("--focusObservation", help="If True, will remove all extra space from observation. Only car race image is left.",
                    type=bool, default = False)
args = parser.parse_args()

obs = EmptyObserver()
SmoothDistDeepQ().run(args.iterations, args.raceCount, args.focusObservation, obs)
